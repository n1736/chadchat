# Frontend

## Project setup

In case npm is installed on your machine, there is no need to run it via docker, except to properly let the dev-server interface with the backend.

```console
$   docker-compose run npm install
```

### Compiles and hot-reloads for development

```console
$   docker-compose run --service-ports npm run serve
```

### Compiles and minifies for production

```console
$   docker-compose run npm run build
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

## Formatting

To keep formatting consistent, format using _Prettier_, which will adhere to the settings set in _package.json_.
