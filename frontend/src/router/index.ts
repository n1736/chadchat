/**
 * Authors: Dominic Martin
 */

import { createRouter, createWebHistory } from 'vue-router';
import { useUserStore } from '@/stores/userStore';
import { useMessageStore } from '@/stores/messageStore';
import { initChat } from '@/stores/utils';

import Dashboard from '@/views/DashboardView.vue';
import Login from '@/views/auth/LoginView.vue';
import Register from '@/views/auth/RegisterView.vue';
import SettingsView from '@/views/auth/SettingsView.vue';
import AllGroupsView from '@/views/groups/AllGroupsView.vue';
import CreateGroupView from '@/views/groups/CreateGroupView.vue';
import NotFoundView from '@/views/errors/NotFoundView.vue';
import GroupProfileView from '@/views/groups/GroupProfileView.vue';
import GroupSettingsView from '@/views/groups/GroupSettingsView.vue';
import { useChatStore } from '@/stores/chatStore';

const routes = [
    {
        path: '/',
        name: 'dashboard',
        component: Dashboard,
        meta: {
            requiresAuth: true,
        },
        beforeEnter: () => {
            const chatStore = useChatStore();

            if (chatStore.chats.length === 0) {
                return { name: 'groups' };
            }
        },
    },
    {
        path: '/chat',
        name: 'chat',
        component: Dashboard,
        meta: {
            requiresAuth: true,
        },
        beforeEnter: () => {
            const chatStore = useChatStore();

            if (chatStore.chats.length === 0) {
                return { name: 'groups' };
            }
        },
    },

    {
        path: '/login',
        name: 'login',
        component: Login,
        meta: {
            inhibitedByAuth: true,
        },
    },
    {
        path: '/register',
        name: 'register',
        component: Register,
        meta: {
            inhibitedByAuth: true,
        },
    },

    {
        path: '/groups/create',
        name: 'create-group',
        component: CreateGroupView,
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: '/settings',
        name: 'settings',
        component: SettingsView,
        meta: {
            requiresAuth: true,
        },
    },

    {
        path: '/groups',
        name: 'groups',
        component: AllGroupsView,
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: '/groups/:id/settings',
        name: 'group-settings',
        component: GroupSettingsView,
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: '/groups/:id/profile',
        name: 'group-profile',
        component: GroupProfileView,
        meta: {
            requiresAuth: true,
        },
    },

    // catch-all route for non existent pages --> 404 page
    {
        path: '/:pathMatch(.*)*',
        name: 'error-404',
        component: NotFoundView,
    },
];

// create the router with history mode
const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
});

// global navigation guard, run before any route is entered
router.beforeEach(async (to) => {
    const userStore = useUserStore();
    const messageStore = useMessageStore();

    // get user from database in case of a page reload
    if (!userStore.loggedIn) {
        await userStore.checkLogin();
    }

    if (userStore.loggedIn && messageStore.connection.websocket == null) {
        // initialize the chat functionality
        const noChats = await initChat();

        if (noChats && (to.name === 'chat' || to.name === 'dashboard')) {
            // redirect to all groups list to find a chat to join
            return { name: 'groups' };
        }
    }

    // redirect to login if user is not logged in and site is only visible when logged in
    if (to.meta.requiresAuth && !userStore.loggedIn) {
        return {
            name: 'login',
            // save the destination to redirect to after login in a uri query
            query: {
                redirect: to.fullPath,
            },
        };
    }

    // redirect to home if routes are visited that are not sensible when logged in
    if (to.meta.inhibitedByAuth && userStore.loggedIn) {
        return {
            name: 'dashboard',
        };
    }
});

export default router;
