/**
 * Authors: Dominic Martin
 */

import 'vue-router';

// extend the vue-router module with an interface for meta tags for type hinting
declare module 'vue-router' {
    interface RouteMeta {
        requiresAuth?: boolean;
        inhibitedByAuth?: boolean;
    }
}
