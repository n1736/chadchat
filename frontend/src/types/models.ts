/**
 * Authors: Dominic Martin
 */

export interface User {
    id: number;
    displayName: string;
    userName: string;
}

export interface Message {
    id?: number;
    chatId: number;
    user: User;
    content: string;
    type: number;
    createdAt: string;
    sentSuccessfully?: boolean;
    referenceId?: string;
    read?: boolean;
}

export interface Chat {
    id: number;
    name: string;
    capacity: number;
    description: string;
    memberCount: number;
    oldestMessageLoaded?: number;
    unreadMessages?: number;
    publicChat: boolean;
    userIsAdmin: boolean;
}
