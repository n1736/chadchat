/**
 * Authors: Dominic Martin
 */

import { Message } from '@/types/models';
import { User } from './models';
import { Chat } from '@/types/models';
import { ErrorType } from './errors';

export type ErrorResponse = {
    data: {
        errors: ErrorType[];
    };
    status: number;
};

export type UserResponse = {
    user: User;
};

export type UsersSearchReponse = {
    users: User[];
    totalResults: number;
};

export type RegisterResponse = {
    username: string;
    password: string;
    displayname: string;
};

export type MessagesResponse = {
    messages: Message[];
};

export type ChatResponse = {
    chat: Chat;
};

export type ChatsResponse = {
    chats: Chat[];
};

export type ChatSearchResponse = {
    chats: Chat[];
    totalResults: number;
};
