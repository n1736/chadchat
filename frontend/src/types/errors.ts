/**
 * Authors: Dominic Martin
 */

import { ErrorResponse } from './responses';

export type ResponseError = {
    response: ErrorResponse;
};

export type ErrorType = {
    code: string;
    message: string;
    field?: string;
};
