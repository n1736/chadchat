/**
 * Authors: Dominic Martin
 */

export type WSConnection = {
    websocket: WebSocket | null;
    failedConnectionAttempts: number;
    reconnectTimeout: number;
};

export type WebsocketMessage = {
    type: string;
    data: any;
};

export type NotificationMessage = {
    type: string;
    chat: any; // this is either a fully blown chat or just id and name
};
