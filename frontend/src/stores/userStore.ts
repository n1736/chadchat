/**
 * Authors: Dominic Martin
 */

import axios from 'axios';
import { UserResponse } from '@/types/responses';
import { ErrorType, ResponseError } from '@/types/errors';
import { defineStore } from 'pinia';
import { User } from '@/types/models';
import { useMessageStore } from './messageStore';
import { useChatStore } from './chatStore';
import { cleanStores, initChat } from './utils';

interface UserState {
    user: User | null;
}

export const useUserStore = defineStore('userStore', {
    state: () =>
        ({
            user: null,
        } as UserState),

    getters: {
        loggedIn(state: UserState) {
            return state.user ? true : false;
        },
    },

    actions: {
        /**
         * Login the user with credentials.
         *
         * Connect to websocket server and load chats
         * on successfull login.
         */
        async login(credentials: { userName: string; password: string }) {
            const messageStore = useMessageStore();
            const chatStore = useChatStore();

            let errors: ErrorType[] = [];

            await axios
                .post<UserResponse>('/login', {
                    userName: credentials.userName,
                    password: credentials.password,
                })
                .then(async (response) => {
                    // successfully logged in
                    this.user = {
                        id: response.data.user.id,
                        displayName: response.data.user.displayName,
                        userName: response.data.user.userName,
                    };

                    // initialize the chat functionality
                    initChat();
                })
                .catch((error: ResponseError) => {
                    errors = error.response.data.errors;
                })
                .catch((error) => {
                    console.log(error);
                });

            return errors;
        },
        /**
         * Check whether the user is still logged in and write them to store.
         */
        async checkLogin() {
            await axios
                .get<UserResponse>('/check-login')
                .then((response) => {
                    this.user = response.data.user;
                })
                .catch((error) => {
                    // make sure an eventual user is removed from the store.
                    this.user = null;
                });
        },
        /**
         * Lougout the user.
         */
        async logout() {
            await axios.post('/logout', {});

            cleanStores();
        },
        /**
         * Update the users information.
         */
        async update(information: { userName: string; displayName: string }) {
            let errors: ErrorType[] = [];

            await axios
                .patch<UserResponse>('/profile', {
                    userName: information.userName,
                    displayName: information.displayName,
                })
                .then((response) => {
                    this.user = response.data.user;
                })
                .catch((error: ResponseError) => {
                    errors = error.response.data.errors;
                })
                .catch((error) => {
                    console.log(error);
                });

            return errors;
        },
        /**
         * Change the users password.
         */
        async changePassword(newPassword: string, currentPassword: string) {
            let errors: ErrorType[] = [];

            await axios
                .post('/change-password', {
                    password: newPassword,
                    oldPassword: currentPassword,
                })
                .catch((error: ResponseError) => {
                    errors = error.response.data.errors;
                })
                .catch((error) => {
                    console.log(error);
                });

            return errors;
        },
        /**
         * Unregisters the user from the site.
         */
        async unregister() {
            const messageStore = useMessageStore();
            const chatStore = useChatStore();

            await axios
                .post('/unregister')
                .catch((error) => console.log(error));

            cleanStores();
        },
    },
});
