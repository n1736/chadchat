/**
 * Authors: Dominic Martin
 */

import axios from 'axios';
import { ErrorType, ResponseError } from '@/types/errors';
import { defineStore } from 'pinia';
import { Message } from '@/types/models';
import { MessagesResponse } from '@/types/responses';
import { useUserStore } from '@/stores/userStore';
import {
    NotificationMessage,
    WebsocketMessage,
    WSConnection,
} from '@/types/websocketMessages';
import { uuid } from 'vue-uuid';
import { useChatStore } from './chatStore';
import { useRoute, useRouter } from 'vue-router';
import { CONFIG } from '@/config';
import { useNotificationStore } from './notificationStore';
import { useI18n } from 'vue-i18n';

interface MessageState {
    messages: Message[];
    connection: WSConnection;
}

export const useMessageStore = defineStore('messageStore', {
    state: () =>
        ({
            messages: [],
            connection: {
                websocket: null,
                failedConnectionAttempts: 0,
                reconnectTimeout: 0,
            },
        } as MessageState),

    getters: {
        /**
         * Get all messages for the specified chat.
         */
        messagesByChatId(state: MessageState) {
            return (chatId: number) =>
                state.messages.filter(
                    (storeMessage) => storeMessage.chatId == chatId
                );
        },
    },

    actions: {
        /**
         * Load messages for the specified chat.
         *
         * Only load the 'amount' specified starting from 'oldestMessageLoaded'
         * or the latest messages if 'oldestMessageLoaded' is 0.
         */
        async loadMessages(chatId: number, amount: number) {
            const chatStore = useChatStore();

            let errors: ErrorType[] = [];

            const chat = chatStore.chatById(chatId);

            if (chat!.oldestMessageLoaded === -1) {
                // don't try to load messages if we're already
                // at the latest message of the chat
                return errors;
            }

            await axios
                .post<MessagesResponse>('/messages/load-by-chat', {
                    chatId: chatId,
                    oldestMessageLoaded: chat!.oldestMessageLoaded || 0,
                    amount: amount,
                })
                .then((response) => {
                    const messages = response.data.messages;

                    if (messages.length > 0) {
                        // Set the oldestMessageLoaded for the given chat to
                        // the last entry of the response array.
                        // This is the latest message.
                        chat!.oldestMessageLoaded =
                            messages[messages.length - 1].id;

                        // store the returned messages
                        messages.forEach((message) => {
                            // set sent flag to true to display sent status
                            // (doesn't matter for messages by other users)
                            message.sentSuccessfully = true;
                        });

                        // insert the messages at the start of the message array
                        // messages has to be inverted before!
                        this.messages.splice(0, 0, ...messages.reverse());
                    } else {
                        // if there are no older messages: deactivate
                        // requesting older messages for this chat
                        chat!.oldestMessageLoaded = -1;
                    }
                })
                .catch((error: ResponseError) => {
                    errors = error.response.data.errors;
                })
                .catch((error) => {
                    console.log(error);
                });

            return errors;
        },
        /**
         * Set the database-id for the message as soon as the server aknowledged receiving it.
         */
        setMessageId(message: { referenceId: string; id: number }) {
            // retrieve the message by the pinged back referenceId
            const storeMessage = this.messages.find(
                (storeMessage) =>
                    storeMessage.referenceId == message.referenceId
            );

            if (storeMessage === undefined) {
                console.error('Message not found.');

                return;
            }

            // set the received database-id, remove referenceId, set the successfully sent flag to true
            storeMessage.id = message.id;
            delete storeMessage.referenceId;
            storeMessage.sentSuccessfully = true;
        },
        /**
         * Send a single message via websockets connection in the specified chat.
         */
        async sendMessage(message: {
            chatId: number;
            content: string;
            type: number;
        }) {
            let errors: ErrorType[] = [];

            const userStore = useUserStore();

            // place the message in the store with status: not sent, await websocket response for further actions on it.
            //
            // referenceId is pinged back from the server along with the actual database-id of the message as soon as it's sent
            // and processed by the server.
            const newMessage: Message = {
                user: userStore.user!,
                referenceId: uuid.v1(),
                chatId: message.chatId,
                content: message.content,
                type: message.type,
                createdAt: new Date(Date.now()).toISOString(),
            };

            this.connection.websocket!.send(
                JSON.stringify({
                    action: 'newMessage',
                    userId: newMessage.user.id,
                    chatId: newMessage.chatId,
                    content: newMessage.content,
                    type: 1,
                    referenceId: newMessage.referenceId,
                })
            );

            // Escape HTMLSpecialChars and replace newlines with breaks.
            // Also preserve inner whitespaces.
            // This is independently done on the server side with the
            // original input for security reasons.
            newMessage.content = newMessage.content
                .trim()
                .replaceAll('&', '&amp;')
                .replaceAll('"', '&quot;')
                .replaceAll('<', '&lt;')
                .replaceAll('>', '&gt;')
                .replaceAll(' ', '&nbsp;')
                .replaceAll('\n', '<br/>');

            // with regexes at this point (and the equivalent in the backend)
            // it would be pretty easy to implement "formatting" of messages
            // eg. bold, italic, etc.

            // insert the new message at the end of the message array
            this.messages.push(newMessage);

            return errors;
        },
        /**
         * React to a websocket event, add the new message to store.
         */
        receiveMessage(message: Message) {
            const userStore = useUserStore();

            // set successfully sent to true for messages sent by the user from another connection.
            if (message.user.id === userStore.user!.id) {
                message.sentSuccessfully = true;
            }

            // insert the new message at the end of the message array
            this.messages.push(message);
        },
        /**
         * Handle notification messages from the server.
         */
        handleNotificationMessage(message: NotificationMessage) {
            const chatStore = useChatStore();
            const notificationStore = useNotificationStore();

            switch (message.type) {
                case 'chatAddition':
                    // user was added to a chat
                    chatStore.chats.push(message.chat);
                    chatStore.chats.sort((a, b) => b.id - a.id);
                    // load the latest chat messages
                    this.loadMessages(message.chat.id, CONFIG.INITIAL_MESSAGES);
                    // display notification
                    notificationStore.displayNotification(
                        notificationStore.i18n('groups.uWereAdded', {
                            group: message.chat.name,
                        }),
                        'info'
                    );
                    break;
                case 'chatRemoval':
                    const chatName = chatStore.chatById(message.chat.id)?.name;
                    // user was removed from a chat
                    chatStore.chats = chatStore.chats.filter(
                        (chat) => chat.id != message.chat.id
                    );

                    if (chatStore.activeChatId === message.chat.id) {
                        if (chatStore.chats.length > 0) {
                            chatStore.activeChatId = chatStore.chats[0].id;
                        } else {
                            chatStore.activeChatId = 0;
                        }
                    }
                    // display notification
                    notificationStore.displayNotification(
                        notificationStore.i18n('groups.uWereRemoved', {
                            group: chatName,
                        }),
                        'error'
                    );
                    break;
            }
        },
        /**
         * Establish the websocket connection with the server, setup event callbacks.
         */
        connectToServer() {
            const wsConnectionString =
                process.env.VUE_APP_BACKEND_WS_ROUTE + '/chat';
            const notificationStore = useNotificationStore();

            const connect = () => {
                this.connection.websocket = new WebSocket(wsConnectionString);

                // send the register message to the server
                this.connection.websocket.onopen = () =>
                    this.connection.websocket!.send(
                        JSON.stringify({
                            action: 'register',
                        })
                    );

                this.connection.websocket.onclose = (event) => {
                    const userStore = useUserStore();

                    // don't do anything if the websocket closed due to logout
                    if (!userStore.loggedIn) {
                        return;
                    }

                    // increment the failed connection attempts
                    this.connection.failedConnectionAttempts++;
                    // try to establish a new connection if there were
                    // less than MAX_CONNECTION_ATTEMPTS failed attempts prior,
                    // wait two seconds before trying to reconnect.
                    if (
                        this.connection.failedConnectionAttempts <=
                        CONFIG.MAX_CONNECTION_ATTEMPTS
                    ) {
                        clearTimeout(this.connection.reconnectTimeout);

                        this.connection.reconnectTimeout = setTimeout(
                            () => connect(),
                            2000
                        );
                    } else {
                        //TODO: give the opportunity to retry
                        // --> this should set failedConnectionAttemps back to 0
                        notificationStore.displayNotification(
                            notificationStore.i18n('websocket.connectionError'),
                            'error'
                        );
                        console.error(
                            'Failed to connect to Websocket server.\nToo many failed attempts.'
                        );
                    }
                };

                this.connection.websocket.onerror = (error) => {
                    console.error(error);
                };

                // setup callbacks for server messages
                this.connection.websocket.onmessage = (event) => {
                    const wsMessage = JSON.parse(
                        event.data
                    ) as WebsocketMessage;

                    switch (wsMessage.type) {
                        case 'chatMessage':
                            if (wsMessage.data.referenceId) {
                                this.setMessageId({
                                    referenceId: wsMessage.data.referenceId,
                                    id: wsMessage.data.id,
                                });
                            } else {
                                this.receiveMessage(wsMessage.data);
                            }
                            break;
                        case 'notificationMessage':
                            this.handleNotificationMessage(wsMessage.data);
                            break;
                        case 'serverMessage':
                            console.log(wsMessage.data);
                            break;
                        case 'error':
                            console.log(wsMessage.data);
                            break;
                        default:
                            console.log(wsMessage);
                    }
                };
            };

            connect();
        },
    },
});
