/**
 * Authors: Dominic Martin
 */

import { CONFIG } from '@/config';
import { useRouter } from 'vue-router';
import { useChatStore } from './chatStore';
import { useMessageStore } from './messageStore';
import { useUserStore } from './userStore';

/**
 * Resets all stores to initial conditions.
 */
export function cleanStores() {
    const userStore = useUserStore();
    const messageStore = useMessageStore();
    const chatStore = useChatStore();

    // remove the user first to prevent websocket from attempting reconnect.
    userStore.user = null;
    // close the websocket connection and remove messages from the store.
    messageStore.connection.websocket!.close();
    messageStore.connection.failedConnectionAttempts = 0;
    messageStore.messages = [];
    // reset chat store
    chatStore.chats = [];
    chatStore.activeChatId = 0;
}

/**
 * Initialize the chat related stores.
 *
 * Used on page reload and directly after login.
 */
export async function initChat() {
    const messageStore = useMessageStore();
    const chatStore = useChatStore();

    let noChats = false;

    // init websocket connection
    messageStore.connectToServer();

    // load all available chats
    await chatStore.loadChats();

    // set the active chat to a specific chat
    // TODO: make this dynamic! (it's the first chat for now)
    if (chatStore.chats.length > 0) {
        chatStore.activeChatId = chatStore.chats[0].id;

        // load messages for the active chat
        messageStore.loadMessages(
            chatStore.activeChatId,
            CONFIG.INITIAL_MESSAGES
        );
    } else {
        chatStore.activeChatId = 0;
        noChats = true;
    }

    return noChats;
}
