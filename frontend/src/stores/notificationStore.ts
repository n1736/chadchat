/**
 * Authors: Adrian Leuteritz
 */

import { defineStore } from 'pinia';
import { CONFIG } from '@/config';

type NotificationType = 'success' | 'error' | 'info';

type notificationState = {
    message: string;
    show: boolean;
    type: NotificationType;
    i18n: any;
};

export const useNotificationStore = defineStore('notificationStore', {
    state: () =>
        ({
            message: '',
            show: false,
            type: 'info',
            i18n: null,
        } as notificationState),
    actions: {
        displayNotification(message: string, type: NotificationType) {
            this.message = message;
            this.show = true;
            this.type = type;

            setTimeout(() => {
                this.show = false;
            }, CONFIG.NOTIFICATION_DURATION);
        },
    },
});
