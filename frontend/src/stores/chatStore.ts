/**
 * Authors: Dominic Martin
 */

import { defineStore } from 'pinia';
import { Chat, User } from '@/types/models';
import { ErrorType, ResponseError } from '@/types/errors';
import {
    ChatResponse,
    ChatSearchResponse,
    ChatsResponse,
    UsersSearchReponse,
} from '@/types/responses';
import axios from 'axios';
import { useMessageStore } from './messageStore';
import { CONFIG } from '@/config';

interface ChatState {
    chats: Chat[];
    activeChatId: number;
    search: string;
    searchResults: Chat[];
    totalResults: number;
}

export const useChatStore = defineStore('chatStore', {
    state: () =>
        ({
            chats: [],
            activeChatId: -1,
            search: '',
            searchResults: [],
            totalResults: 0,
        } as ChatState),
    getters: {
        /**
         * Search for a chat matching 'name' locally in chats.
         */
        searchChat(state) {
            return (name: string) => {
                //TODO: implement fuzzy search
                return state.chats.filter((chat) => chat.name.includes(name));
            };
        },
        /**
         * Determine wheter the user has joined the specified chat.
         */
        myChat(state) {
            return (id: number) => {
                return state.chats.find((chat) => chat.id === id) != undefined;
            };
        },
        /**
         * Return the chat specified by the id.
         */
        chatById(state) {
            return (id: number) => {
                return state.chats.find((chat) => chat.id === id);
            };
        },
    },
    actions: {
        /**
         * Load all chats the user has joined.
         */
        async loadChats() {
            let errors: ErrorType[] = [];

            await axios
                .get<ChatsResponse>('/chats')
                .then((response) => {
                    this.chats = response.data.chats;
                })
                .catch((error: ResponseError) => {
                    errors = error.response.data.errors;
                })
                .catch((error) => {
                    console.log(error);
                });

            return errors;
        },
        /**
         * Search for chats matching the 'name'.
         *
         * Paginated by amount and offset.
         */
        async searchAvailableChats(
            name: string,
            amount: number,
            offset: number
        ) {
            this.search = name;

            let errors: ErrorType[] = [];
            await axios
                .get<ChatSearchResponse>(
                    `/chats/search?name=${name}&amount=${amount}&offset=${offset}`
                )
                .then((response) => {
                    this.searchResults = response.data.chats;
                    this.totalResults = response.data.totalResults;
                })
                .catch((error: ResponseError) => {
                    errors = error.response.data.errors;
                })
                .catch((error) => {
                    console.log(error);
                });

            return errors;
        },
        /**
         * Join the specified chat and add it to the local chats with addidtional information.
         */
        async joinChat(id: number) {
            let errors: ErrorType[] = [];

            await axios
                .post<ChatResponse>(`/chats/${id}/join`, {})
                .then((response) => {
                    const messageStore = useMessageStore();

                    this.chats.push(response.data.chat);
                    // to avoid confusion, the chats are sorted by id
                    // which is the same sequence they are returned in
                    // when explicitly loaded
                    this.chats.sort((a, b) => b.id - a.id);
                    // also remove the chat from the search result list
                    this.searchResults = this.searchResults.filter(
                        (chat) => chat.id != id
                    );
                    // load latest chat messages
                    messageStore.loadMessages(
                        response.data.chat.id,
                        CONFIG.INITIAL_MESSAGES
                    );
                })
                .catch((error: ResponseError) => {
                    errors = error.response.data.errors;
                })
                .catch((error) => {
                    console.log(error);
                });

            return errors;
        },
        /**
         * Leave the specified chat and remove it from the local chats.
         */
        async leaveChat(id: number) {
            let errors: ErrorType[] = [];

            await axios
                .post<ChatResponse>(`/chats/${id}/leave`, {})
                .then((response) => {
                    this.chats = this.chats.filter((chat) => chat.id != id);
                })
                .catch((error: ResponseError) => {
                    errors = error.response.data.errors;
                })
                .catch((error) => {
                    console.log(error);
                });

            return errors;
        },
        /**
         * Create a new chat and append the created chat to the store.
         */
        async create(
            name: string,
            description: string,
            capacity: number,
            publicChat: boolean,
            initialMembers: number[]
        ) {
            let errors: ErrorType[] = [];

            await axios
                .post<ChatResponse>('/chats/create', {
                    name: name,
                    description: description,
                    capacity: capacity,
                    publicChat: publicChat,
                })
                .then(async (response) => {
                    const chat = response.data.chat;

                    // add the selected initial members to the created group
                    await this.addMembers(chat.id, initialMembers);

                    // store the new chat locally
                    this.chats.push(chat);

                    // set the freshly created chat to active
                    this.activeChatId = chat.id;
                })
                .catch((error: ResponseError) => {
                    errors = error.response.data.errors;
                })
                .catch((error) => {
                    console.log(error);
                });

            return errors;
        },
        /**
         * Update the specified chat.
         */
        async update(
            id: number,
            name: string,
            description: string,
            capacity: number,
            publicChat: boolean,
            members: number[]
        ) {
            let errors: ErrorType[] = [];

            await axios
                .patch<ChatResponse>(`/chats/${id}`, {
                    name: name,
                    description: description,
                    capacity: capacity,
                    publicChat: publicChat,
                })
                .then(async (response) => {
                    let chat = this.chatById(id);
                    chat = response.data.chat;
                })
                .catch((error: ResponseError) => {
                    errors = error.response.data.errors;
                })
                .catch((error) => {
                    console.log(error);
                });

            return errors;
        },
        /**
         * Delete the specified chat.
         */
        async delete(id: number) {
            let errors: ErrorType[] = [];

            await axios
                .delete(`/chats/${id}`)
                .then((response) => {
                    // remove the chat from the local store
                    this.chats = this.chats.filter((chat) => chat.id === id);
                })
                .catch((error: ResponseError) => {
                    errors = error.response.data.errors;
                })
                .catch((error) => {
                    console.log(error);
                });

            return errors;
        },
        /**
         * Add users by id to the specified chat.
         */
        async addMembers(chatId: number, userIds: number[]) {
            let errors: ErrorType[] = [];

            await axios
                .post(`/chats/${chatId}/add-members`, {
                    userIds: userIds,
                })
                .then((response) => {
                    // TODO: maybe actually do something with the response
                    // (depends on whether members will be saved in local store)
                })
                .catch((error: ResponseError) => {
                    errors = error.response.data.errors;
                })
                .catch((error) => {
                    console.log(error);
                });

            return errors;
        },
        /**
         * Remove the specified user from the specified chat.
         */
        async removeMember(chatId: number, userId: number) {
            let errors: ErrorType[] = [];

            await axios
                .post(`/chats/${chatId}/remove-member`, {
                    userId: userId,
                })
                .then((response) => {
                    // TODO: maybe actually do something with the response
                    // (depends on whether members will be saved in local store)
                })
                .catch((error: ResponseError) => {
                    errors = error.response.data.errors;
                })
                .catch((error) => {
                    console.log(error);
                });

            return errors;
        },
        /**
         * Search for users available as new members to the specified chat.
         *
         * Paginated by amount and offset.
         *
         * If id == 0: don't filter for a specific chat.
         */
        async searchAvailableMembers(
            id: number,
            name: string,
            amount: number,
            offset: number
        ) {
            let users: User[] = [];

            await axios
                .get<UsersSearchReponse>(
                    `/chats/${id}/search-available-users?name=${name}&amount=${amount}&offset=${offset}`
                )
                .then((response) => {
                    users = response.data.users;
                })
                .catch((error) => {
                    // TODO: actually do something with the error
                    console.log(error);
                });

            return users;
        },
        /**
         * Load the members for the specified chat and return them.
         *
         * Paginated by amount and offset.
         *
         * They won't be stored locally.
         */
        async loadMembers(id: number, amount: number, offset: number) {
            let members: User[] = [];

            await axios
                .get<UsersSearchReponse>(
                    `/chats/${id}/members?amount=${amount}&offset=${offset}`
                )
                .then((response) => {
                    members = response.data.users;
                })
                .catch((error: ResponseError) => {
                    // TODO: actually do something with the error
                    console.log(error);
                })
                .catch((error) => {
                    console.log(error);
                });

            return members;
        },
    },
});
