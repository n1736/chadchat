/**
 * Authors: Adrian Leuteritz, Dominic Martin
 */

import axios from 'axios';

// inject the route to the backend api from environment to be used as base  url in all requests
axios.defaults.baseURL = process.env.VUE_APP_BACKEND_API_ROUTE;
// allow credential cookies
axios.defaults.withCredentials = true;

import { createApp } from 'vue';
import App from './App.vue';
import './assets/tailwind.css';
import { createPinia } from 'pinia';
import router from './router';
import i18nInit from './lang';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
// FontAwesome icons
import {
    faPaperPlane,
    faTimes,
    faUser,
    faUsers,
    faFlag,
    faCrown,
} from '@fortawesome/free-solid-svg-icons';

// add icons to library
library.add(faPaperPlane, faTimes, faUser, faUsers, faFlag, faCrown);

// language selection should take place somewhere here
let lang = 'de';

// init internationalization engine with de as default locale
const i18n = i18nInit('de');

// create and mount the vue instance to the #app-div
createApp(App)
    .use(createPinia())
    .use(router)
    .use(i18n)
    .component('fas', FontAwesomeIcon)
    .mount('#app');
