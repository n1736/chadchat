<!--
    Authors: Adrian Leuteritz, Dominic Martin
-->

<template>
    <div class="flex flex-col items-center justify-start w-full h-full">
        <div
            class="flex items-center w-11/12 justify-start rounded-t-2xl bg-cat-green-grey h-[60px] px-3"
        >
            <router-link
                :to="{
                    name: activeChat?.userIsAdmin
                        ? 'group-settings'
                        : 'group-profile',
                    params: { id: chatId },
                }"
                :title="
                    activeChat?.userIsAdmin
                        ? $t('groups.edit')
                        : $t('groups.header')
                "
                class="flex items-center justify-start space-x-3 text-2xl text-cat-white font-koulen"
            >
                <div
                    class="flex items-center justify-center flex-none w-[40px] h-[40px] rounded-full bg-cat-light-grey"
                >
                    <fas class="h-5 text-cat-white" icon="fa-solid fa-users" />
                </div>
                <span>
                    {{ activeChat?.name }}
                </span>
                <fas
                    v-if="activeChat?.userIsAdmin"
                    class="h-5 text-cat-white"
                    icon="fa-solid fa-crown"
                />
            </router-link>
        </div>
        <div
            class="flex flex-col items-center justify-center w-11/12 h-[calc(100%-100px)] p-2 mb-4 bg-cat-grey rounded-b-2xl"
        >
            <div
                class="flex flex-col w-full p-3 space-y-5 bg-gradient-to-l from-gray-400 to-gray-800 rounded-t-2xl h-[calc(100%-80px)]"
            >
                <div
                    class="flex flex-col w-full pr-3 space-y-3 overflow-y-auto scrollbar-thin scrollbar-track-gray-500 scrollbar-thumb-gray-800"
                    ref="messageContainer"
                >
                    <message-component
                        v-if="chatId != -1"
                        v-for="message in messages"
                        :key="message.id"
                        :content="message.content"
                        :type="message.type"
                        :userName="message.user.userName"
                        :displayName="message.user.displayName"
                        :createdAt="message.createdAt"
                        :successfullySent="message.sentSuccessfully"
                        :ownMessage="message.user.id === userStore.user!.id"
                        :ref="'message-' + message.id"
                        class="w-full h-full sm:w-3/4 lg:w-1/2 rounded-2xl"
                    />
                </div>
            </div>
            <div
                class="flex justify-center w-full h-20 py-4 bg-cat-dark-grey rounded-b-2xl"
            >
                <form
                    class="flex w-10/12 h-max max-h-32"
                    @submit.prevent="sendMessage()"
                    ref="messageForm"
                    @keydown.shift="shiftPressed = true"
                    @keyup.shift="shiftPressed = false"
                >
                    <textarea
                        class="flex-grow w-5/6 p-3 resize-none rounded-l-2xl"
                        rows="1"
                        :placeholder="$t('chat.messagePlaceholder')"
                        v-model="newMessageContent"
                        @keyup.enter="handleEnter()"
                    />
                    <button
                        type="submit"
                        :disabled="
                            newMessageContent.trim().replaceAll('\n', '') ==
                                '' || sending
                        "
                        class="flex-grow w-1/6 p-3 transition-colors bg-cat-button-color rounded-r-2xl disabled:bg-cat-light-grey disabled:text-cat-grey"
                        ref="messageFormSubmit"
                    >
                        <fas icon="fa-solid fa-paper-plane" />
                    </button>
                </form>
            </div>
        </div>
    </div>
</template>

<script lang="ts">
import { Options, Vue } from 'vue-class-component';
import MessageComponent from './MessageComponent.vue';
import { useMessageStore } from '@/stores/messageStore';
import { useUserStore } from '@/stores/userStore';
import { watch } from 'vue';
import { useChatStore } from '@/stores/chatStore';
import { CONFIG } from '@/config';

@Options({
    components: { MessageComponent },
    props: {
        chatId: Number,
    },
})
export default class ChatComponent extends Vue {
    chatId!: number;

    messageStore = useMessageStore();
    userStore = useUserStore();
    chatStore = useChatStore();

    newMessageContent = '';
    newMessageType = 1; // message type hardcoded to default for now
    sending = false;

    shiftPressed = false;

    loadingMessages = false;

    get messages() {
        return this.messageStore.messagesByChatId(this.chatId);
    }

    get activeChat() {
        return this.chatStore.chatById(this.chatId);
    }

    async sendMessage() {
        this.sending = true;

        await this.messageStore.sendMessage({
            chatId: this.chatId,
            content: this.newMessageContent,
            type: this.newMessageType,
        });

        this.newMessageContent = '';
        this.sending = false;
    }

    scrollToLatestMessage() {
        const messageContainer: HTMLElement = this.$refs
            .messageContainer as HTMLElement;
        messageContainer.scrollTop = messageContainer.scrollHeight;
    }

    loadAdditionalMessages() {
        this.loadingMessages = true;

        // save the old scroll height of the message container
        const messageContainer = this.$refs.messageContainer as HTMLElement;
        const oldMessageContainerHeight = messageContainer.scrollHeight;

        // load additional Messages
        this.messageStore
            .loadMessages(this.chatId, CONFIG.ADDITIONAL_MESSAGES)
            .finally(() => {
                // scroll to the former top of the scroll height of the message container
                // (This ensures the former oldest message of the chat will seemingly stay
                // in the position it was.)
                this.$nextTick(() => {
                    messageContainer.scrollTop =
                        messageContainer.scrollHeight -
                        oldMessageContainerHeight;
                });

                this.loadingMessages = false;
            });
    }

    mounted() {
        // setup a watcher to scroll to the bottom if there's a new message appended
        // and it did not happen via chunk-loading (additional) messages
        watch(
            () => this.messages.length,
            (newLength, oldLength) => {
                if (newLength > oldLength && !this.loadingMessages) {
                    this.$nextTick(() => this.scrollToLatestMessage());
                }
            }
        );

        // setup an event listener to load additional messages if scrolled to the top
        (this.$refs.messageContainer as HTMLElement).onwheel = (event) => {
            const messageContainer = this.$refs.messageContainer as HTMLElement;
            if (
                !this.loadingMessages &&
                messageContainer.scrollTop == 0 &&
                event.deltaY < 0
            ) {
                this.loadAdditionalMessages();
            }
        };
    }

    handleEnter() {
        // don't submit the form if shift is pressed (new line),
        // it's already sending a message or there's no message to be sent.
        if (
            this.shiftPressed ||
            this.sending ||
            this.newMessageContent.trim().replaceAll('\n', '') == ''
        ) {
            return;
        }

        (this.$refs.messageForm as HTMLFormElement).requestSubmit();
    }
}
</script>
