/**
 * Authors: Dominic Martin
 */

export const CONFIG = {
    // max. number of initial messages to load in a chat
    INITIAL_MESSAGES: 10,
    // max. number of messages to load when loading additional
    // messages
    ADDITIONAL_MESSAGES: 10,
    // max. number of results to display in quick search panel
    QUICK_SEARCH_GROUPS: 3,
    // max. number of results to display per page in full search view
    FULL_SEARCH_GROUPS: 10,
    // max. number of users to display as search results
    // when searching for available members for a group
    USER_SEARCH_RESULTS: 10,
    // max. number of connection attempts before aborting
    MAX_CONNECTION_ATTEMPTS: 5,
    // number of ms to show a notification for
    NOTIFICATION_DURATION: 3000,
};
