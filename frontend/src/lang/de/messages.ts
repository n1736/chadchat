export default {
    // Succesful responses
    '0001': 'Erfolgreich registriert!',
    '0002': 'Erfolgreich eingeloggt.',
    '0004': 'Erfolgreich ausgeloggt.',
    '0005': 'Neue Gruppe {groupName} erfolgreich erstellt.',
    '0006': '{user} zur Gruppe hinzugefügt.',
    '0012': 'Du bist der Gruppe {groupName} erfolgreich beigetreten!',
    '0013': 'Du bist aus der Gruppe {groupName} erfolgreich ausgetreten!',
    '0016': 'Neue Gruppe {groupName} erfolgreich erstellt.',
    '0017': 'Teilnehmer erfolgreich hinzugefügt.',
    '0019': 'Passwort erfolgreich geändert.',
    '0020': 'Gruppe erfolgreich aktualisiert.',
    '0021': 'Gruppe erfolgreich gelöscht.',
    '0022': 'Benutzerdaten erfolgreich aktualisiert.',
    '0023': 'Benutzer erfolgreich gelöscht.',
    '0024': 'Teilnehmer erfolgreich entfernt.',

    // Authentification errors
    '1001': 'Der Benutzername ist leider bereits vergeben.',
    '1002': 'Anmeldedaten nicht korrekt. Bitte versuche es noch einmal.',
    '1003': 'Session nicht valide.',
    '1004': 'Session abgelaufen.',
    '1006': 'Falsches Passwort',
    '1007': 'Du bist nicht Teil der Gruppe.',

    // Membership errors
    '2001': 'Du bist der Gruppe {groupName} bereits beigetreten.',
    '2002': 'Du bist der Gruppe {groupName} bereits ausgetreten.',
    '2003': 'Du bist nicht Admin der Gruppe {groupName}.',

    // Input errors
    '3001': 'Gruppenname ist zu kurz.',
    '3002': 'Benutzername ist zu kurz.',
    '3003': 'Passwort ist zu kurz.',
    '3004': 'Anzeigename ist zu kurz.',
    '3005': 'Gruppenbeschreibung ist zu lang.',
};
