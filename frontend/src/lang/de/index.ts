import messages from './messages';
import ui from './ui';

export default {
    de: {
        ...messages,
        ...ui,
    },
};
