import { createI18n } from 'vue-i18n';
import de from './de';
import en from './en';

const i18nInit = (locale: string) => {
    return createI18n({
        locale: locale,
        fallbackLocale: 'de',
        messages: {
            ...de,
            ...en,
        },
    });
};

export default i18nInit;
