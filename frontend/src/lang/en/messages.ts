export default {
    // Succesful responses
    '0001': 'Registered successfully!',
    '0002': 'Logged in successfully.',
    '0003': 'Login data correct.',
    '0004': 'Logged out successfully.',
    '0005': 'New group {groupName} successfully created.',
    '0006': '{user} added to group.',
    '0012': 'You have successfully joined the group {groupName}!',
    '0013': 'You have successfully left the group {groupName}!',
    '0016': 'New group {groupName} successfully created.',
    '0017': 'Member added successfully.',
    '0019': 'Password changed successfully.',
    '0020': 'Group updated succesfully.',
    '0021': 'Group deleted succesfully.',
    '0022': 'User data updated succesfully.',
    '0023': 'User deleted succesfully.',
    '0024': 'Member removed succesfully.',

    // Authentification errors
    '1001': 'Unfortunately, the username is already taken.',
    '1002': 'Login data not correct. Please try again',
    '1003': 'Session not valid.',
    '1004': 'Session expired.',
    '1005': 'Session ended.',
    '1006': 'Wrong password',
    '1007': 'You are not a member of the group.',

    // Membership errors
    '2001': 'You have already joined the group {groupName}.',
    '2002': 'You have already left the group {groupName}.',
    '2003': 'You are not the admin of the group {groupName}.',

    // Input errors
    '3001': 'Name is too short.',
    '3002': 'Username is too short.',
    '3003': 'Password is too short.',
    '3004': 'Displayname is too short.',
    '3005': 'Description is too long',
};
