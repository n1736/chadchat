import messages from './messages';
import ui from './ui';

export default {
    en: {
        ...messages,
        ...ui,
    },
};
