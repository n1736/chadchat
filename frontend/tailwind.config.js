/**
 *     Authors: Justin Donath, Adrian Leuteritz
 */

module.exports = {
    content: ['./public/**/*.html', './src/**/*.{vue,js,ts}'],
    theme: {
        fontFamily: {
            sans: ['Open Sans', 'sans-serif'],
            koulen: ['Koulen', 'cursive'],
        },
        extend: {
            colors: {
                'cat-white': '#d9d9d9',
                'cat-light-grey': '#7d7d7d',
                'cat-grey': '#4C4E52',
                'cat-green-grey': '#3c4245',
                'cat-dark-grey': '#36393f',
                'cat-hover-group-color': '#9ca3af',
                'cat-hover-button-color': '#4b73e1',
                'cat-button-color': '#4BBEE1',
                'cat-green': '#4BE1B9',
                'cat-red': '#E04B72',
            },
        },
    },
    plugins: [require('tailwind-scrollbar')],
    variants: {
        extend: {
            scrollbar: ['rounded', 'opacity'],
        },
    },
};
