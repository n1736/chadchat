"""
    Authors: Jan Sallermann
"""

from sqlalchemy import Table, Column, Integer, String, MetaData, Boolean
from migrate.changeset.constraint import ForeignKeyConstraint


def upgrade(migrate_engine):
    meta = MetaData(bind=migrate_engine)
    chatrooms = Table('chatrooms', meta, autoload=True)
    description = Column('description', String(4096))
    description.create(chatrooms)


def downgrade(migrate_engine):
    meta = MetaData(bind=migrate_engine)
    chatrooms = Table('chatrooms', meta, autoload=True)
    chatrooms.c.description.drop()
