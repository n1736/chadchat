"""
    Authors: Jan Sallermann
"""

from sqlalchemy import Table, Column, Integer, String, MetaData, Boolean
from packages.database.database import get_db
from packages.database.schema import UserModel


def upgrade(migrate_engine):
    meta = MetaData(bind=migrate_engine)
    users = Table('users', meta, autoload=True)
    display_col = Column('display_name', String(30))
    display_col.create(users)
    db_session = get_db().session
    users = db_session.query(UserModel).all()
    for user in users:
        user.display_name = user.name
    db_session.commit()
    db_session.close()


def downgrade(migrate_engine):
    meta = MetaData(bind=migrate_engine)
    users = Table('users', meta, autoload=True)
    users.c.display_name.drop()
