"""
    Authors: Jan Sallermann
"""

from sqlalchemy import Table, Column, Integer, String, MetaData, Boolean
from migrate.changeset.constraint import ForeignKeyConstraint


def upgrade(migrate_engine):
    meta = MetaData(bind=migrate_engine)
    users = Table('users', meta, autoload=True)
    chatrooms = Table('chatrooms', meta, autoload=True)
    admin_col = Column('admin_id', Integer)
    public_col = Column('public', Boolean)
    admin_col.create(chatrooms)
    public_col.create(chatrooms)
    fkey_cons = ForeignKeyConstraint([chatrooms.c.admin_id], [users.c.id])
    fkey_cons.create()


def downgrade(migrate_engine):
    meta = MetaData(bind=migrate_engine)
    chatrooms = Table('chatrooms', meta, autoload=True)
    users = Table('users', meta, autoload=True)
    fkey_cons = ForeignKeyConstraint([chatrooms.c.admin_id], [users.c.id])
    fkey_cons.drop()
    chatrooms.c.admin_id.drop()
    chatrooms.c.public.drop()
