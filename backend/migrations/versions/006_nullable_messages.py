"""
    Authors: Jan Sallermann
"""

from sqlalchemy import Table, Column, Integer, String, MetaData, Boolean
from migrate.changeset.constraint import ForeignKeyConstraint


def upgrade(migrate_engine):
    meta = MetaData(bind=migrate_engine)
    messages = Table('messages', meta, autoload=True)
    messages.c.user_id.alter(nullable=True)


def downgrade(migrate_engine):
    #Nothing to do since we dont want it non nullable again
    pass
