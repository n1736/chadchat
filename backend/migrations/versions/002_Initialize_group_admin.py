"""
    Authors: Jan Sallermann
"""

from sqlalchemy import Table, Column, Integer, String, MetaData
from packages.database.database import get_db
from packages.database.schema import ChatroomModel


def upgrade(migrate_engine):
    meta = MetaData(bind=migrate_engine)
    db_session = get_db().session
    chats = db_session.query(ChatroomModel).all()
    for chat in chats:
        if not chat.admin_id:
            chat.admin_id = 1
        if not chat.public and chat.public != 0:
            chat.public = True
    db_session.commit()
    db_session.close()



def downgrade(migrate_engine):
    pass
