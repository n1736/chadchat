"""
    Authors: Dominic Martin
"""

from sqlalchemy import Table, Column, Integer, String, MetaData, Boolean
from packages.database.database import get_db
from packages.database.schema import ChatroomModel


def upgrade(migrate_engine):
    meta = MetaData(bind=migrate_engine)
    chatrooms = Table('chatrooms', meta, autoload=True)
    try:
        chatrooms.c.member_count.drop()
    except AttributeError:
        return



def downgrade(migrate_engine):
    # nothing to do, it's gone
    pass
