# Migration

What you need to do when the db changes.

## 1. Create a change script

```console
$ docker-compose run backend python manage_db.py script "<Name of file>"
```

## 2. Edit the change script

Edit the file at migration_catchad/version/`Name of File`

### 2.1 to create Tables

```python
from sqlalchemy import Table, Column, Integer, String, MetaData

meta = MetaData()

account = Table(
    'account', meta,
    Column('id', Integer, primary_key=True),
    Column('login', String(40)),
    Column('passwd', String(40)),
)


def upgrade(migrate_engine):
    meta.bind = migrate_engine
    account.create()


def downgrade(migrate_engine):
    meta.bind = migrate_engine
    account.drop()
```

First you define a variable with the whanted table structure.
In the **upgrade** function you create the table.
Please also write the **downgrade** function, this function is needed for future downgrades.

In the downgrade function you need to write the oposit action of upgdade, so when you create a table in upgrade you need to drop the table in the downgrade function.

### 2.2 Modifying existing tables

Creat a change script as describt in **1.**

```python
from sqlalchemy import Table, MetaData, String, Column


def upgrade(migrate_engine):
    meta = MetaData(bind=migrate_engine)
    account = Table('account', meta, autoload=True)
    emailc = Column('email', String(128))
    emailc.create(account)


def downgrade(migrate_engine):
    meta = MetaData(bind=migrate_engine)
    account = Table('account', meta, autoload=True)
    account.c.email.drop()
```

The autoload option reference to the existing schema.

---

For more table options visit [the SQLAlchemy Migrate Docs.](https://https://sqlalchemy-migrate.readthedocs.io/en/latest/changeset.html#changeset-system)

## 3. Test the change script

Run

```console
$ docker-compose run backend python manage_db.py test
```

## 4. Apply the Upgrade

```console
$ docker-compose run backend python manage_db.py upgrade
```

or with the version option

```console
$ docker-compose run backend python manage_db.py upgrade --version
```

---

Examples from SQLAlchemy Migrate Docs
