"""
    Authors: Dominic Martin
"""

from packages.database.schema import ChatroomModel, UserChatroomModel
from packages.database.database import get_db
from packages.auth_controller import AuthController

dbs = get_db().session
controller = AuthController()

# create some users
user_names = ["Hans", 'Klaus', 'Justin', 'Jan', 'Dieter']
for user_name in user_names:
    request = {'userName': user_name, 'password': '123456789'}
    controller.register(request)

# create some chats
chat_names = ["Sachsen", "Cat-Haven", 'BA', 'Dresden']
for chat_name in chat_names:
    chat = ChatroomModel(name=chat_name, capacity=30, public=True, admin_id=1)
    dbs.add(chat)
    dbs.commit()

# assign users to chats
entry = UserChatroomModel(user_id=1, chat_id=1)
dbs.add(entry)
dbs.commit()
entry = UserChatroomModel(user_id=2, chat_id=1)
dbs.add(entry)
dbs.commit()
entry = UserChatroomModel(user_id=3, chat_id=1)
dbs.add(entry)
dbs.commit()
entry = UserChatroomModel(user_id=4, chat_id=1)
dbs.add(entry)
dbs.commit()
entry = UserChatroomModel(user_id=5, chat_id=1)
dbs.add(entry)
dbs.commit()
entry = UserChatroomModel(user_id=1, chat_id=2)
dbs.add(entry)
dbs.commit()
entry = UserChatroomModel(user_id=1, chat_id=3)
dbs.add(entry)
dbs.commit()
