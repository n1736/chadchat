"""
    Authors: Erik Luckner, Dominic Martin
"""

import json, os
from packages.helper import get_chat_by_id
from fastapi import WebSocket, WebSocketDisconnect, Depends, FastAPI
from fastapi.middleware.cors import CORSMiddleware
from typing import List, Tuple, Dict
from packages.database.database import get_db
from packages.database.schema import ChatroomModel, UserModel, UserChatroomModel
from packages.auth_controller import AuthController
from packages.message_controller import MessageController
from packages.constants.status_codes import STATUS_CODES


FRONTEND_ROUTE = os.environ['FRONTEND_BASE_URL']

app = FastAPI()


origins = [
    FRONTEND_ROUTE,
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


class ConnectionManager:
    def __init__(self):
        self.all_active_connections: List[Tuple[WebSocket, int]] = []
        self.subscriptions: Dict[int, List[WebSocket]] = {}

        db_session = get_db().session
        chatrooms = db_session.query(ChatroomModel).all()
        db_session.close()

        for room in chatrooms:
            self.subscriptions[room.id] = []

    def add_chatroom(self, chatroom_id: int, initial_users: List[int]):
        """
        Adds the initial user of a chatroom to the new created chatroom
        Parameters:
            chatroom_id     [int]               -   the id of the new chatroom
            initial_user    List[int]           -   a list of WebSocket sessions
        """
        self.subscriptions[chatroom_id] = list(initial_users)

    def remove_chatroom(self, chatroom_id: int):
        """
        Delet the data for the chatroom in subscriptions
        all WebSocketsession to the room will be removed
        Parameters:
            chatroom_id     [int]               -   the id of the chatroom
        """
        del self.subscriptions[chatroom_id]

    async def connect(self, websocket: WebSocket, user_id: int):
        """
        Accept the WebSocketsession and append the session to
            all_active_connections
        Parameters:
            websocket       [WebSocket]         -   the sessionid of the WebSocket
                                                    connection
            user_id         [int]               -   the userid from the joining WebSocketsession
        """
        await websocket.accept()
        self.all_active_connections.append((websocket, user_id))

    async def disconnect(self, websocket: WebSocket, user_id: int):
        """
        handles the disconnect for a WebSocketsession, remove session from
            all_active_connection and delet every entry in subscriptions for
            the WebSocket
            Close the WebSocketsession
        Parameters:
            websocket       [WebSocket]         -   the WebSocketsession
            user_id         [int]               -   the id of the user who disconnect
        """
        self.all_active_connections.remove((websocket, user_id))
        for chat_id in self.subscriptions:
            chatroom = self.subscriptions[chat_id]
            if websocket in chatroom:
                chatroom.remove(websocket)
        try:
            await websocket.close()
        except:
            return

    def subscribe(self, user_id: int, websocket: WebSocket):
        """
        Adds the user initialy to the chatrooms
        Parameters:
            user_id         [int]               -   the user id
            websocket       [WebSocket]         -   the WebSocketsession from the joining
                                                    user
        """
        db_session = get_db().session
        subscriptions = db_session.query(UserChatroomModel).filter(
            UserChatroomModel.user_id == user_id).all()
        db_session.close()
        for subscription in subscriptions:
            self.subscriptions[subscription.chat_id].append(websocket)

    async def add_subscription(self, user_id: int, chat_id: int, 
                               send_notification = False):
        """
        Adds the user to a chatroom, when he joints
        Parameters:
            user_id         [int]               -   the user id
            chat_id         [int]               -   the id of the chatroom
        """
        for connection in self.all_active_connections:
            websocket, connection_user_id = connection
            if connection_user_id == user_id:
                self.subscriptions[chat_id].append(websocket)
                if send_notification:
                    await self.__send_addition_notification(chat_id, websocket)

    def remove_subscription(self, user_id: int, chat_id: int):
        """
        Removes a user/WebSocketsession from a chatroom
        Parameters:
            user_id         [int]               -   the user id
            chat_id         [int]               -   the id of the chatroom
        """
    async def remove_subscription(self, user_id: int, chat_id: int,
                                  send_notification = False):
        """
        Removes a user/WebSocketsession from a chatroom
        Parameters:
            user_id         [int]               -   the user id
            chat_id         [int]               -   the id of the chatroom
        """
        for connection in self.all_active_connections:
            websocket, connection_user_id = connection
            if connection_user_id == user_id:
                self.subscriptions[chat_id].remove(websocket)
                if send_notification:
                    await self.__send_removal_notification(chat_id, websocket)
                
    async def __send_addition_notification(self, chat_id: int, websocket):
        db_session = get_db().session
        chat = get_chat_by_id(chat_id, db_session)

        message = json.dumps({
            'type': 'notificationMessage', 
            'data': {
                'type': 'chatAddition',
                'chat': {
                    'id': chat.id,
                    'name': chat.name,
                    'capacity': chat.capacity,
                    'memberCount': len(chat.users),
                    'publicChat': chat.public,
                    'description': chat.description,
                    'userIsAdmin': False
        }}})

        await websocket.send_text(message)

    async def __send_removal_notification(self, chat_id: int, websocket):
        db_session = get_db().session
        chat = get_chat_by_id(chat_id, db_session)

        message = json.dumps({
            'type': 'notificationMessage',
            'data': {
                'type': 'chatRemoval',
                'chat': {
                    'id': chat.id,
                    'name': chat.name
        }}})

        await websocket.send_text(message)


    async def broadcast(self, message: str, websocket: WebSocket, chat_id: int):
        """
        send data to all user of a chatroom except the creater
        Parameters:
            message         [str]               -   the data which is send
            websocket       [WebSocket]         -   the WebSocketsession which will not
                                                    receive the data
            chat_id         [int]               -   the cjatroom id to select the receiver
        """
        for connection in self.all_active_connections:
            active_websocket, user_id = connection
            if active_websocket != websocket and\
                    websocket in self.subscriptions[chat_id]:
                try:
                    await active_websocket.send_text(message)
                except WebSocketDisconnect:
                    await manager.disconnect(websocket=websocket, user_id=user_id)
            else:
                continue

    @staticmethod
    def verify_websocket(websocket: WebSocket):
        """
        Verify the websocket connection attempt is coming from an authenticated user.
        """

        controller = AuthController()
        return controller.verify_websocket(websocket=websocket, db_session=get_db().session)


manager = ConnectionManager()


async def register(auth_user, websocket):
    manager.subscribe(
        user_id=auth_user.id, websocket=websocket)
    json_response = json.dumps({'type': 'serverMessage',
                                'data': {'message': "Connected", 'code': 0}})
    await websocket.send_text(json_response)


async def send_new_message(auth_user, json_data, websocket):
    controller = MessageController()
    (message_object, response_code) = controller.write_msg_to_db(
        auth_user, json_data['chatId'], json_data['content'],
        json_data['type']
    )

    response = STATUS_CODES[response_code]
    if int(response_code) < 1000:

        # broadcast the message to all channel members except the sender
        await manager.broadcast(message=json.dumps(message_object),
                                websocket=websocket,
                                chat_id=message_object['data']['chatId'])

        # reflect the message with referenceId back to the client who sent it
        message_object['data']['referenceId'] = json_data['referenceId']
        json_response = json.dumps(dict(message_object, **response))
    else:
        json_response = json.dumps({'type': 'error', 'data': response})
    await websocket.send_text(json_response)


@app.websocket("/chat")
async def chat(websocket: WebSocket,
               auth_user_id: int = Depends(ConnectionManager.verify_websocket)):
    await manager.connect(websocket=websocket, user_id=auth_user_id)

    db_session = get_db().session
    auth_user = db_session.query(
        UserModel).filter(UserModel.id == auth_user_id).first()
    db_session.close()

    try:
        while True:
            input = await websocket.receive_text()
            json_data = json.loads(input)

            if json_data["action"]:
                if json_data['action'] == "register":
                    await register(auth_user, websocket)
                    continue

                if (websocket, auth_user_id) in manager.all_active_connections:
                    if json_data['action'] == "newMessage" and\
                            json_data['chatId'] and\
                            json_data['content'] and\
                            json_data['type'] and\
                            json_data['referenceId']:
                        await send_new_message(auth_user, json_data, websocket)
                        continue

                    if json_data['action'] == "leave":
                        await manager.disconnect(websocket=websocket, user_id=auth_user_id)
                        break

                    json_response = json.dumps(
                        {'type': 'error',
                         'data': {'message': "action not suported"}})
                    await websocket.send_text(json_response)

            else:
                json_response = json.dumps(
                    {'message': 'Missing variables', 'code': 1})
                await websocket.send_json(json_response)
                await manager.disconnect(websocket=websocket, user_id=auth_user_id)

    except WebSocketDisconnect:
        await manager.disconnect(websocket=websocket, user_id=auth_user_id)
