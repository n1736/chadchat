"""
    Authors: Jan Sallermann
"""

import os
from packages.database.database import get_db
from fastapi import Request, Depends
from fastapi.middleware.cors import CORSMiddleware
from websocket import app
from packages.auth_controller import AuthController
from packages.message_controller import MessageController
from packages.chat_controller import ChatController
from packages.user_controller import UserController


def verify_request(request: Request):
    db_session = get_db().session
    controller = AuthController()
    return controller.verify_request(request, db_session)


@app.get("/hello_world")
async def root():
    return {"message": "Hello World"}


@app.post("/register")
async def register(request: Request):
    request = await request.json()
    controller = AuthController()
    return controller.register(request)


@app.post("/login")
async def login(request: Request):
    request = await request.json()
    controller = AuthController()
    return controller.login(request)


@app.post("/logout")
async def logout(request: Request, auth: bool = Depends(verify_request)):
    token = request.cookies.get('Authorization')
    controller = AuthController()
    return controller.logout(token)


@app.get("/check-login")
async def check_login(request: Request, auth: bool = Depends(verify_request)):
    token = request.cookies.get('Authorization')
    controller = AuthController()
    return controller.check_login(token)


@app.post("/messages/load-by-chat")
async def message_load_by_chat(request: Request,
                               auth: bool = Depends(verify_request)):
    token = request.cookies.get('Authorization')
    request = await request.json()
    controller = MessageController()
    return controller.load_by_chat(request, token)


@app.get("/chats")
async def get_chat_for_user(request: Request,
                            auth: bool = Depends(verify_request)):
    token = request.cookies.get('Authorization')
    controller = ChatController()
    return controller.get_chats_for_user(token)


@app.get("/chats/search")
async def search_chats(request: Request, auth: bool = Depends(verify_request)):
    params = request.query_params
    token = request.cookies.get('Authorization')
    controller = ChatController()
    return controller.search_non_membership_chats(token, params)


@app.post("/chats/create")
async def create_chat(request: Request, auth: bool = Depends(verify_request)):
    token = request.cookies.get('Authorization')
    request = await request.json()
    controller = ChatController()
    return await controller.create_chat(request, token)


@app.patch("/chats/{id}")
async def update_chat(request: Request, auth: bool = Depends(verify_request)):
    chat_id = int(request.path_params['id'])
    token = request.cookies.get('Authorization')
    request = await request.json()
    controller = ChatController()
    return controller.update_chat(request, token, chat_id)


@app.delete("/chats/{id}")
async def delete_chat(request: Request, auth: bool = Depends(verify_request)):
    chat_id = int(request.path_params['id'])
    token = request.cookies.get('Authorization')
    controller = ChatController()
    return await controller.delete_chat(token, chat_id)


@app.get("/chats/{id}/members")
async def get_members(request: Request, auth: bool = Depends(verify_request)):
    chat_id = int(request.path_params['id'])
    params = request.query_params
    controller = ChatController()
    return controller.get_chat_members(params, chat_id)


@app.post("/chats/{id}/join")
async def join_chat(request: Request, auth: bool = Depends(verify_request)):
    chat_id = int(request.path_params['id'])
    token = request.cookies.get('Authorization')
    controller = ChatController()
    return await controller.join_chat(token, chat_id)


@app.post("/chats/{id}/leave")
async def leave_chat(request: Request, auth: bool = Depends(verify_request)):
    chat_id = int(request.path_params['id'])
    token = request.cookies.get('Authorization')
    controller = ChatController()
    return await controller.leave_chat(token, chat_id)


@app.post("/chats/{id}/add-members")
async def add_members(request: Request, auth: bool = Depends(verify_request)):
    token = request.cookies.get('Authorization')
    chat_id = int(request.path_params['id'])
    request = await request.json()
    controller = ChatController()
    return await controller.add_members(request, token, chat_id)


@app.post("/chats/{id}/remove-member")
async def remove_member(request: Request, auth: bool = Depends(verify_request)):
    token = request.cookies.get('Authorization')
    chat_id = int(request.path_params['id'])
    request = await request.json()
    controller = ChatController()
    return await controller.remove_member(request, token, chat_id)


@app.get("/chats/{id}/search-available-users")
async def search_available_users(request: Request,
                                auth: bool = Depends(verify_request)):
    params = request.query_params
    token = request.cookies.get('Authorization')
    chat_id = int(request.path_params['id'])
    controller = ChatController()
    return controller.search_available_users(params, token, chat_id)


@app.post("/change-password")
async def change_pw(request: Request, auth: bool = Depends(verify_request)):
    token = request.cookies.get('Authorization')
    request = await request.json()
    controller = UserController()
    return controller.change_pw(request, token)


@app.patch("/profile")
async def update_user(request: Request, auth: bool = Depends(verify_request)):
    token = request.cookies.get('Authorization')
    request = await request.json()
    controller = UserController()
    return controller.update_user(request, token)


@app.delete("/profile")
async def delete_user(request: Request, auth: bool = Depends(verify_request)):
    token = request.cookies.get('Authorization')
    controller = UserController()
    return await controller.delete_user(token)
