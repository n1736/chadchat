"""
    Authors: Dominic Martin
"""

import uvicorn
from api import app as api_app
#from websocket import app as ws_app

if(__name__ == '__main__'):
    # TODO: remove in the future or better:
    #       move to seeder
    # for testing   // drop all tables before using
    #               // only use once
    # import seed_test_data

    uvicorn.run(api_app, host="0.0.0.0", port=80)
