"""
    Authors: Jan Sallermann
"""

import os
import json
from starlette.responses import Response
from fastapi import HTTPException
from packages.database.schema import UserModel
from packages.constants.status_codes import STATUS_CODES


class Controller():
    def __init__(self):
        pass

    def create_response(self, code: str, data: dict = {}):
        """
        Create a standard response for successful exit codes

        Parameters:
            code            [str]               -   Internal status code
            data            [dict]              -   optional additional data

        Return:
                            [Response]          -   HTTP response
        """
        data = dict(data, **dict(STATUS_CODES[code], **{'code': code}))
        data = json.dumps(data)
        status = STATUS_CODES[code]['status_code']
        return Response(content=data, status_code=status, media_type="application/json")

    def create_error(self, errors: list):
        """
        Create a error response for unsuccessful exit codes

        Parameters:
            errors          [list]               -   List of all error codes

        Return:
                            [Response]          -   HTTP response
        """
        data = {'errors': []}
        for error in errors:
            data['errors'].append(dict(STATUS_CODES[error], **{'code': error}))
        data = json.dumps(data)
        status = STATUS_CODES[errors[0]]['status_code']
        return Response(content=data, status_code=status, media_type="application/json")

    def check_request(self, request, check_list):
        """
        Check the input parameters for a request to ensure sound execution

        Parameters:
            request         [json]              -   HTTP request as json
            check_list      [list]              -   list of required input keys

        """
        for entry in check_list:
            try:
                check = request[entry]
            except:
                raise HTTPException(status_code=400, detail="Invalid input")
