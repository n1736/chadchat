"""
    Authors: Jan Sallermann
"""

import jwt
import datetime
from packages.database.schema import (
    UserModel, UserSessionModel, ChatroomModel
)


def create_token(name, pw):
    """
    Create a java-web-token by passing the username, the currnet timestamp
    and the password to the encode

    Return:
                        [str]               -   encoded java-web-token
    """
    data = {'name': name,
            'timestamp': str(datetime.datetime.now())}
    token = jwt.encode(data, pw)
    return token


def get_session_by_token(token, db_session):
    """
    Get the user session from the database for a given token

    Parameters:
        token           [str]               -   java-web-token
        db_session      [Session]           -   database session

    Return:
                        [UserSessionModel]  -   database entry of the
                                                user_session
    """
    user_session = db_session.query(UserSessionModel).filter(
        UserSessionModel.token == token).first()
    return user_session


def validate_session(token: str, db_session):
    """
    Check if the user_session is valid for the given token.

    Parameters:
        token           [str]           -   java-web-token
        db_session      [Session]           -   database session

    Return:
                        [str]           -   Code whether the user_session
                                            is valid or not
    """
    user_session = get_session_by_token(token, db_session)
    if not user_session:
        return '1003'
    if user_session.logout_date != None:
        return '1005'
    if user_session.expire_date < datetime.datetime.now():
        user_session.logout_date = datetime.datetime.now()
        db_session.commit()
        return '1004'
    return '0008'


def get_user_by_id(user_id, db_session):
    """
    Get the user from a given Id

    Parameters:
        user_id         [int]               -   Id of the user
        db_session      [Session]           -   database session

    Return:
                        [UserModel]         -   database entry of the
                                                user
    """
    return db_session.query(UserModel).filter(UserModel.id == user_id).first()


def get_chat_by_id(chat_id, db_session):
    """
    Get the chatroom from a given Id

    Parameters:
        token           [str]               -   Id of the chat
        db_session      [Session]           -   database session

    Return:
                        [ChatroomModel]     -   database entry of the
                                                chatroom
    """
    return db_session.query(ChatroomModel).filter(
        ChatroomModel.id == chat_id).first()
