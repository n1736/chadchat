"""
    Authors: Jan Sallermann
"""

STATUS_CODES = {
    #Succesful responses
    '0001': {
        'status_code': 201,
        'message': 'User created in DB.'},
    '0002': {
        'status_code': 200,
        'message': 'Login was successful'},
    '0003': {
        'status_code': 200,
        'message': 'Credentials are valid.'},
    '0004': {
        'status_code': 200,
        'message': 'Successfully logged out'},
    '0005': {
        'status_code': 201,
        'message': 'Chatroom created'},
    '0006': {
        'status_code': 200,
        'message': 'User added to chatroom'},
    '0007': {
        'status_code': 200,
        'message': 'User is logged in'},
    '0008': {
        'status_code': 200,
        'message': 'Session is valid'},
    '0009': {
        'status_code': 200,
        'message': 'New messages loaded by chat'},
    '0010': {
        'status_code': 200,
        'message': 'Chats loaded for user'},
    '0011': {
        'status_code': 200,
        'message': 'Chat search successful'},
    '0012': {
        'status_code': 200,
        'message': 'Joined chat successfully'},
    '0013': {
        'status_code': 200,
        'message': 'Left chat successfully'},
    '0014': {
        'status_code': 200,
        'message': 'Message wrote to db'},
    '0015': {
        'status_code': 200,
        'message': 'User search successful'},
    '0016': {
        'status_code': 201,
        'message': 'Chatroom created successfully'},
    '0017': {
        'status_code': 200,
        'message': 'Members added successfully'},
    '0018': {
        'status_code': 200,
        'message': 'Chat members returned'},
    '0019': {
        'status_code': 200,
        'message': 'Password changed successfully'},
    '0020': {
        'status_code': 200,
        'message': 'Chatroom updated succesfully'},
    '0021': {
        'status_code': 204,
        'message': 'Chatroom deleted succesfully'},
    '0022': {
        'status_code': 200,
        'message': 'User data updated succesfully'},
    '0023': {
        'status_code': 204,
        'message': 'User deleted succesfully'},
    '0024': {
        'status_code': 204,
        'message': 'User removed succesfully'},

    #Authentification errors
    '1001': {
        'status_code': 400,
        'message': 'User already exists.',
        'field': 'userName'},
    '1002': {
        'status_code': 401,
        'message': 'Wrong credentials',
        'field': 'userName, password' },
    '1003': {
        'status_code': 401,
        'message': 'Invalid token'},
    '1004': {
        'status_code': 401,
        'message': 'Session expired'},
    '1005': {
        'status_code': 401,
        'message': 'Session logged out'},
    '1006': {
        'status_code': 401,
        'message': 'Wrong password',
        'field': 'oldPassword'},
    '1007': {
        'status_code': 401,
        'message': 'User is not part of the chat'},

    #Membership errors
    '2001': {
        'status_code': 400,
        'message': 'User is already member of the chatroom'},
    '2002': {
        'status_code': 401,
        'message': 'User does not belong to the chatroom'},
    '2003': {
        'status_code': 401,
        'message': 'User is not admin of the chatroom'},
    '2004': {
        'status_code': 400,
        'message': 'User is already part of the chatroom'},

    #Input errors
    '3001': {
        'status_code': 400,
        'message': 'Name of chatroom too short'},
    '3002': {
        'status_code': 400,
        'message': 'Invalid register input, invalid length for userName',
        'field': 'userName' },
    '3003': {
        'status_code': 400,
        'message': 'Invalid register input, invalid length for password',
        'field': 'password' },
    '3004': {
        'status_code': 400,
        'message': 'Invalid register input, invalid length for displayName',
        'field': 'displayName' },
    '3005': {
        'status_code': 400,
        'message': 'Chatroom description is too long',
        'field': 'description' },
}
