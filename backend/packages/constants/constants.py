"""
    Authors: Jan Sallermann
"""

SALT = b'Rf\x8cH\x89\xde$_4\x9f;\xe8\xab\x90\x11$\x85\xf4/\xeb\x10\xc2\xccJ\x0f"\xb5\xfe\xbe\x83\xe7\''
MIN_USER_LENGTH = 2
MAX_USER_LENGTH = 30
MIN_PW_LENGTH = 8
MAX_PW_LENGTH = 70
MAX_DESCRIPTION_LENGTH = 4096
