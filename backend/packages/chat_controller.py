"""
    Authors: Dominic Martin, Jan Sallermann
"""

from packages.controller import Controller
from packages.helper import get_session_by_token, get_chat_by_id
from packages.database.database import get_db
from packages.database.schema import (ChatroomModel, UserChatroomModel,
                                      UserModel, MessageModel)
from packages.constants.constants import MAX_DESCRIPTION_LENGTH
from websocket import manager
from sqlalchemy import and_, text, or_


class ChatController(Controller):
    def __init__(self):
        super().__init__()

    def get_chats_for_user(self, token):
        """
        Get all chats for the user that he belongs to.

        Parameters:
            token           [str]           -   java-web-token

        Return:
                            [Response]      -   HTTP response
        """
        db_session = get_db().session
        user_id = get_session_by_token(token, db_session).user_id
        chats = db_session.query(ChatroomModel).join(
            UserChatroomModel, UserChatroomModel.chat_id == ChatroomModel.id
        ).filter(UserChatroomModel.user_id == user_id).all()
        data = {'chats': []}
        for chat in chats:
            member_count = (len(chat.users))
            user_is_admin = chat.admin_id == user_id
            tmp = {
                'id': chat.id,
                'name': chat.name,
                'capacity': chat.capacity,
                'memberCount': member_count,
                'publicChat': chat.public,
                'description': chat.description,
                'userIsAdmin': user_is_admin
            }
            data['chats'].append(tmp)
        return self.create_response('0010', data)

    def search_non_membership_chats(self, token, params):
        """
        Search through all chats thet the user is not a part of.

        Parameters:
            token           [str]           -   java-web-token
            params          [dict]          - serach parameters

        Return:
                            [Response]      -   HTTP response
        """
        amount = int(params['amount'])
        offset = int(params['offset'])
        search = params['name']

        db_session = get_db().session
        user = get_session_by_token(token, db_session).user
        query = db_session.query(ChatroomModel).filter(
            ChatroomModel.public==True)

        # get all the ids of the users chats in a list
        user_chats = [room.chat_id for room in user.chatrooms]
        #select only chats the user has not joined
        query = query.filter(ChatroomModel.id.not_in(user_chats))

        if search:
            #filter by ci search keyword and order by 'best matching' result
            query = query.filter(ChatroomModel.name.ilike("%"+search+"%"))\
                .order_by(text(f"LOCATE('{search}', name)"))

        res_amount = query.count()

        if amount:
            #Paginate results
            query = query.limit(amount).offset(offset)

        chats = query.all()

        data = {'chats': [], 'totalResults': res_amount}
        for chat in chats:
            member_count = (len(chat.users))
            user_is_admin = chat.admin_id == user.id
            tmp = {
                'id': chat.id,
                'name': chat.name,
                'capacity': chat.capacity,
                'memberCount': member_count,
                'publicChat': chat.public,
                'description': chat.description,
                'userIsAdmin': user_is_admin
            }
            data['chats'].append(tmp)
        return self.create_response('0011', data)

    async def join_chat(self, token, chat_id):
        """
        The user that made the request joins a given chat.

        Parameters:
            token           [str]           -   java-web-token
            chat_id         [int]           -   Id of the chat

        Return:
                            [Response]      -   HTTP response
        """
        db_session = get_db().session
        user_id = get_session_by_token(token, db_session).user_id

        check = db_session.query(UserChatroomModel).filter(and_(
            UserChatroomModel.chat_id == chat_id,
            UserChatroomModel.user_id == user_id
        )).count()

        if check == 0:
            entry = UserChatroomModel(user_id=user_id, chat_id=chat_id)
            db_session.add(entry)
            db_session.commit()

            await manager.add_subscription(user_id, chat_id)

            chat = get_chat_by_id(chat_id, db_session)
            member_count = (len(chat.users))
            user_is_admin = chat.admin_id == user_id
            data = {'chat': {
                'id': chat.id,
                'name': chat.name,
                'capacity': chat.capacity,
                'memberCount': member_count,
                'publicChat': chat.public,
                'description': chat.description,
                'userIsAdmin': user_is_admin
            }}
            db_session.close()
            return self.create_response('0012', data)
        else:
            db_session.close()
            return self.create_error(['2004'])

    async def leave_chat(self, token, chat_id):
        """
        The user that made the request leaves a given chat.

        Parameters:
            token           [str]           -   java-web-token
            chat_id         [int]           -   Id of the chat

        Return:
                            [Response]      -   HTTP response
        """
        db_session = get_db().session
        user_id = get_session_by_token(token, db_session).user_id
        db_entry = db_session.query(UserChatroomModel).filter(and_(
            UserChatroomModel.chat_id == chat_id,
            UserChatroomModel.user_id == user_id
        )).first()
        if not db_entry == None:
            db_session.delete(db_entry)
            db_session.commit()

            await manager.remove_subscription(user_id, chat_id)

        db_session.close()
        return self.create_response('0013')

    async def create_chat(self, request, token):
        """
        Create a chatroom.

        Parameters:
            request         [json]          -   HTTP request as json
            token           [str]           -   java-web-token

        Return:
                            [Response]      -   HTTP response
        """
        db_session = get_db().session
        self.check_request(request, ['name', 'capacity', 'publicChat',
                                     'description']),
        errors = []
        if not len(request['name']) > 1:
            errors.append('3001')
        if len(request['description']) > MAX_DESCRIPTION_LENGTH:
            errors.append('3005')
        if len(errors) != 0:
            return self.create_error(errors)

        admin_id = get_session_by_token(token, db_session).user_id

        chat = ChatroomModel(
            name=request['name'],
            capacity=request['capacity'],
            public=request['publicChat'],
            description=request['description'],
            admin_id=admin_id
        )

        db_session.add(chat)
        db_session.commit()
        manager.add_chatroom(chat.id, chat.users)

        await self.join_chat(token, chat.id)

        member_count = (len(chat.users))
        data = {'chat': {
            'id': chat.id,
            'name': chat.name,
            'capacity': chat.capacity,
            'memberCount': member_count,
            'publicChat': chat.public,
            'description': chat.description,
            'userIsAdmin': True
        }}
        db_session.close()
        return self.create_response('0016', data)

    def update_chat(self, request, token, chat_id):
        """
        Update the data of a given chatroom, only works for the admin of the
        chatroom

        Parameters:
            request         [json]          -   HTTP request as json
            token           [str]           -   java-web-token
            chat_id         [int]           -   Id of the chat

        Return:
                            [Response]      -   HTTP response
        """
        db_session = get_db().session
        self.check_request(request, ['name', 'capacity', 'publicChat',
                                     'description']),
        errors = []
        if not len(request['name']) > 1:
            errors.append('3001')
        if len(request['description']) > MAX_DESCRIPTION_LENGTH:
            errors.append('3005')
        chat = get_chat_by_id(chat_id, db_session)
        user_id = get_session_by_token(token, db_session).user_id

        if chat.admin_id != user_id:
            errors.append('2003')
        if len(errors) != 0:
            return self.create_error(errors)

        chat.name = request['name']
        chat.capacity = request['capacity']
        chat.public = request['publicChat']
        chat.description = request['description']
        member_count = (len(chat.users))
        data = {'chat': {
            'id': chat.id,
            'name': chat.name,
            'capacity': chat.capacity,
            'memberCount': member_count,
            'publicChat': chat.public,
            'description': chat.description,
            'userIsAdmin': True
        }}

        db_session.commit()
        db_session.close()
        return self.create_response('0016', data)

    async def delete_chat(self, token, chat_id):
        """
        Delete a given chatroom, only works for the admin of the chatroom

        Parameters:
            token           [str]           -   java-web-token
            chat_id         [int]           -   Id of the chat

        Return:
                            [Response]      -   HTTP response
        """
        db_session = get_db().session
        chat = get_chat_by_id(chat_id, db_session)
        user_id = get_session_by_token(token, db_session).user_id

        if chat.admin_id != user_id:
            return self.create_error(['2003'])

        for user in chat.users:
           await manager.remove_subscription(user.user_id, chat_id)
        manager.remove_chatroom(chat_id)

        db_session.query(MessageModel).filter(
            MessageModel.chat_id == chat_id).delete()
        db_session.query(UserChatroomModel).filter(
            UserChatroomModel.chat_id == chat_id).delete()

        db_session.delete(chat)
        db_session.commit()
        db_session.close()
        return self.create_response('0021')

    async def add_members(self, request, token, chat_id):
        """
        Add memebers to a chatroom, only works for the admin of a chatroom

        Parameters:
            request         [json]          -   HTTP request as json
            token           [str]           -   java-web-token

        Return:
                            [Response]      -   HTTP response
        """
        db_session = get_db().session
        self.check_request(request, ['userIds']),
        chat = get_chat_by_id(chat_id, db_session)
        user_id = get_session_by_token(token, db_session).user_id

        if chat.admin_id != user_id:
            return self.create_error(['2003'])

        members = []
        for user_id in request['userIds']:
            tmp = UserChatroomModel(
                chat_id = chat.id,
                user_id = user_id)
            members.append(tmp)
            await manager.add_subscription(user_id, chat_id,
                                           send_notification=True)

        db_session.add_all(members)
        db_session.commit()
        db_session.close()
        return self.create_response('0017')

    def search_available_users(self, params, token, chat_id):
        """
        Change the password for the user.

        Parameters:
            request         [json]          -   HTTP request as json
            token           [str]           -   java-web-token
            chat_id         [int]           -   Id of the chat

        Return:
                            [Response]      -   HTTP response
        """
        amount = int(params['amount'])
        offset = int(params['offset'])
        search = params['name']

        db_session = get_db().session
        user_id = get_session_by_token(token, db_session).user_id
        query = db_session.query(UserModel).filter(UserModel.id != user_id)

        if chat_id != 0:
            chat = get_chat_by_id(chat_id, db_session)
            # get all the ids of the users chats in a list
            user_chats = [user.user_id for user in chat.users]
            #select only chats the user has not joined
            query = query.filter(UserModel.id.not_in(user_chats))

        if search:
            #filter by ci search keyword and order by 'best matching' result
            query = query.filter(UserModel.name.ilike("%"+search+"%"))\
                .order_by(text(f"LOCATE('{search}', name)"))

        res_amount = query.count()

        if amount:
            #Paginate results
            query = query.limit(amount).offset(offset)

        users = query.all()

        data = {'users': [], 'totalResults': res_amount}
        for user in users:
            tmp = {'userName': user.name,
                   'displayName': user.display_name,
                   'id': user.id}
            data['users'].append(tmp)
        db_session.close()
        return self.create_response('0015', data)

    def get_chat_members(self, params, chat_id):
        """
        Get all memebers of a chatroom.

        Parameters:
            params          [dict]          - serach parameters
            chat_id         [int]           -   Id of the chat

        Return:
                            [Response]      -   HTTP response
        """
        amount = int(params['amount'])
        offset = int(params['offset'])

        db_session = get_db().session
        chat = get_chat_by_id(chat_id, db_session)
        user_chats = [user.user_id for user in chat.users]

        query = db_session.query(UserModel).filter(
            UserModel.id.in_(user_chats))

        res_amount = query.count()

        if amount:
            #Paginate results
            query = query.limit(amount).offset(offset)

        users = query.all()

        data = {'users': [], 'totalResults': res_amount}
        for user in users:
            tmp = {'userName': user.name,
                   'displayName': user.display_name,
                   'id': user.id}
            data['users'].append(tmp)
        db_session.close()
        return self.create_response('0018', data)

    async def remove_member(self, request, token, chat_id):
        """
        Remove a chatroom-member, only works for the admin of the chatroom.

        Parameters:
            request         [json]          -   HTTP request as json
            token           [str]           -   java-web-token
            chat_id         [int]           -   Id of the chat

        Return:
                            [Response]      -   HTTP response
        """
        db_session = get_db().session
        self.check_request(request, ['userId']),
        chat = get_chat_by_id(chat_id, db_session)
        active_user_id = get_session_by_token(token, db_session).user_id

        if chat.admin_id != active_user_id:
            return self.create_error(['2003'])

        db_entry = db_session.query(UserChatroomModel).filter(and_(
            UserChatroomModel.chat_id == chat_id,
            UserChatroomModel.user_id == request['userId']
        )).first()
        if not db_entry == None:
            db_session.delete(db_entry)
            db_session.commit()

            await manager.remove_subscription(request['userId'], chat_id,
                                              send_notification=True)

        db_session.close()
        return self.create_response('0024')
