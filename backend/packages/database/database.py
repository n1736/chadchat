"""
    Authors: Erik Luckner
"""

import string
import sqlalchemy.orm
import importlib
from sqlalchemy.orm.session import Session
from sqlalchemy.orm import sessionmaker, scoped_session
from packages.database.schema import dal
from sqlalchemy import inspect
import os


DB = os.environ['DB_NAME']
DB_USER = os.environ['DB_USER']
DB_PW = os.environ['DB_PASSWORD']
DB_PORT = os.environ['DB_PORT']


class BaseDatabaseService:
    def __init__(self, session: Session = Session()):
        self.session = session

    def create_database_session(self):
        """
        Connect to the database given by the connection_string and create
        a session.

        Parameters:
            connection_string   [str]   -   String containing the necessary
                                            date to connect to the database
        """
        connection_string = f"mysql+pymysql://{DB_USER}:{DB_PW}@mariadb:{DB_PORT}/{DB}"
        dal.connection = connection_string
        dal.connect()

        dal.s_session = scoped_session(sessionmaker(bind=dal.engine))
        dal.session = dal.s_session()
        dal.s_session.registry.clear()

        self.session: sqlalchemy.orm.session.Session = dal.session

    def create_tables(self, tables: list):
        """
        Create the given list of database tables, which match the class names
        in schema.py.

        Parameters:
            tables          [list]      -   List of tables names matching the
                                            class names in schema.py
        """
        for table in tables:
            if not inspect(dal.engine).has_table(table):
                table_model = importlib.import_module(
                    'packages.database.schema')
                orm_table = getattr(table_model, table)
                orm_table.__table__.create(bind=dal.engine, checkfirst=True)


def get_db():
    database = BaseDatabaseService()
    database.create_database_session()
    return(database)
