"""
    Authors: Erik Luckner
"""

from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy_utils import generic_repr
from sqlalchemy import (
    Column, Integer, Text, String, Date, create_engine,
    ForeignKey, DateTime, PrimaryKeyConstraint, Boolean
)


Base = declarative_base()


@generic_repr
class UserModel(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, nullable=False)
    name = Column(String(30), nullable=False)
    display_name = Column(String(30), nullable=False)
    password = Column(String(300), nullable=False)
    creation_date = Column(Date, nullable=False)

    # Relationships
    sessions = relationship("UserSessionModel", back_populates="user")
    messages = relationship("MessageModel", back_populates="user")
    chatrooms = relationship("UserChatroomModel", back_populates="user")
    owned_chatrooms = relationship("ChatroomModel", back_populates="admin")


class UserSessionModel(Base):
    __tablename__ = "user_sessions"

    id = Column(Integer, primary_key=True, nullable=False)
    user_id = Column(Integer, ForeignKey(UserModel.id), nullable=False)
    token = Column(Text, nullable=False)
    login_date = Column(DateTime, nullable=False)
    logout_date = Column(DateTime)
    expire_date = Column(DateTime, nullable=False)

    # Relationships
    user = relationship("UserModel", back_populates="sessions")


class ChatroomModel(Base):
    __tablename__ = "chatrooms"

    id = Column(Integer, primary_key=True, nullable=False)
    name = Column(String(30), nullable=False)
    capacity = Column(Integer, nullable=False)
    public = Column(Boolean, nullable=False)
    description = Column(String(4096))
    admin_id = Column(Integer, ForeignKey(UserModel.id))

    # Relationships
    users = relationship("UserChatroomModel", back_populates="chatroom")
    messages = relationship("MessageModel", back_populates="chatroom")
    admin = relationship("UserModel", back_populates="owned_chatrooms")


class UserChatroomModel(Base):
    __tablename__ = "users_chatrooms"

    user_id = Column(Integer, ForeignKey(UserModel.id), nullable=False)
    chat_id = Column(Integer, ForeignKey(ChatroomModel.id), nullable=False)

    # Relationships
    user = relationship("UserModel", back_populates="chatrooms")
    chatroom = relationship("ChatroomModel", back_populates="users")

    __table_args__ = (PrimaryKeyConstraint(
        'user_id', 'chat_id', name='_user_chat_uc'),)


class MessageModel(Base):
    __tablename__ = "messages"

    id = Column(Integer, primary_key=True, nullable=False)
    user_id = Column(Integer, ForeignKey(UserModel.id))
    chat_id = Column(Integer, ForeignKey(ChatroomModel.id), nullable=False)
    content = Column(String(1000), nullable=False)
    creation_date = Column(DateTime, nullable=False)
    type = Column(Integer, nullable=False)

    # Relationships
    user = relationship("UserModel", back_populates="messages")
    chatroom = relationship("ChatroomModel", back_populates="messages")


class DataAccessLayer:
    def __init__(self):
        self.connection = ''
        self.engine = None
        self.Session = None
        self.session = None

    def connect(self):
        self.engine = create_engine(self.connection)
        Base.metadata.bind = self.engine
        Base.metadata.create_all(self.engine)
        self.session = sessionmaker(bind=self.engine)


dal = DataAccessLayer()
