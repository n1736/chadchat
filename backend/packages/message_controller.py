"""
    Authors: Erik Luckner, Jan Sallermann
"""

import datetime
from packages.controller import Controller
from packages.helper import get_user_by_id, get_session_by_token
from packages.database.database import get_db
from packages.database.schema import MessageModel, UserChatroomModel
from sqlalchemy import desc
import html


class MessageController(Controller):
    def __init__(self):
        super().__init__()

    def load_by_chat(self, request, token):
        """
        Load a given amout of messages for a chat that are older than a given
        message.

        Parameters:
            request         [json]          -   HTTP request as json
            token           [str]           -   java-web-token

        Return:
                            [Response]      -   HTTP response
        """
        self.check_request(
            request, ['chatId', 'oldestMessageLoaded', 'amount'])
        db_session = get_db().session
        auth_user = get_session_by_token(token, db_session).user

        if db_session.query(UserChatroomModel).filter(
            UserChatroomModel.chat_id == request["chatId"],
            UserChatroomModel.user_id == auth_user.id
        ).count() != 1:
            return self.create_error(['1007'])

        query = db_session.query(MessageModel).filter(
            MessageModel.chat_id == request['chatId'])

        if request['oldestMessageLoaded'] > 0:
            query = query.filter(
                MessageModel.id < request['oldestMessageLoaded'])

        messages = query.order_by(desc(MessageModel.id)).limit(
            request['amount']).all()
        data = {'messages': []}
        for message in messages:
            user = get_user_by_id(message.user_id, db_session)
            tmp = {
                'id': message.id,
                'chatId': message.chat_id,
                'user': {
                    'id': user.id,
                    'userName': user.name,
                    'displayName': user.display_name},
                'content': message.content,
                'type': message.type,
                'createdAt': message.creation_date.isoformat()+'Z',
                'read': False
            }
            data['messages'].append(tmp)
        return self.create_response('0009', data)

    def __create_message_object(self, auth_user, new_message):
        """
        Create a message object as it is expected by the websocket connection.

        Parameters:
            auth_user       [UserModel]     -   user that created the message
            new_message     [MessageModel]  -   message

        Return:
                            [dict]          -   message object
        """
        message_object = {
            'type': 'chatMessage',
            'data': {
                'id': new_message.id,
                'user': {'id': auth_user.id,
                            'userName': auth_user.name,
                            'displayName': auth_user.display_name},
                'chatId': new_message.chat_id, 'content': new_message.content,
                'type': new_message.type,
                'createdAt': new_message.creation_date.isoformat() + 'Z'
            }
        }
        return message_object

    def write_msg_to_db(self, auth_user, chat_id, content: str, msg_type):
        """
        Write a new message to the database.

        Parameters:
            auth_user       [UserModel]     -   user that created the message
            chat_id         [int]           -   Id of the chat that the message
                                                was written in
            content         [content]       -   content of the message
            msg_type        [int]           -   type of the message, currently
                                                only text messages

        Return:
                            [dict]          -   message object
                            [str]           -   internal response code
        """
        db_session = get_db().session
        if db_session.query(UserChatroomModel).filter(
            UserChatroomModel.user_id == auth_user.id,
            UserChatroomModel.chat_id == chat_id
        ).count() != 0:
            creation_date = datetime.datetime.now()

            # escape HTMLSpecialChars, preserve whitespaces
            # and convert newlines to breaks
            messageContent = str(html.escape(content.lstrip('\n').rstrip('\n').strip()))
            messageContent = messageContent.replace(' ', '&nbsp;')
            messageContent = messageContent.replace('\n', '<br/>')

            message = MessageModel(user_id=auth_user.id,
                                   chat_id=chat_id,
                                   content=messageContent,
                                   creation_date=creation_date,
                                   type=msg_type)
            message_object = self.__create_message_object(auth_user, message)
            db_session.add(message)
            db_session.commit()
            db_session.close()
            return (message_object, '0014')

        db_session.close()
        return (None, '2002')

