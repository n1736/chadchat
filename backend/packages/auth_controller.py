"""
    Authors: Jan Sallermann
"""

import os
import datetime
from packages.database.schema import UserModel, UserSessionModel
from packages.database.database import get_db
from packages.controller import Controller
from packages.helper import (
    create_token, get_session_by_token, validate_session
)
from fastapi import HTTPException
from packages.constants.status_codes import STATUS_CODES
from packages.constants.constants import (SALT, MIN_PW_LENGTH, MIN_USER_LENGTH,
                                          MAX_USER_LENGTH, MAX_PW_LENGTH)
import hashlib as hl


class AuthController(Controller):
    def __init__(self):
        super().__init__()

    def verify_request(self, request, db_session) -> bool:
        """
        Verify if the cookie is still valid for the request.

        Parameters:
            request         [str]               -   HTTP request
            db_session      [Session]           -   database session

        Return:
                            [bool]              -   Whether the cookie is valid
                                                    or not
        """
        try:
            token = request.cookies.get('Authorization')
            code = validate_session(token, db_session)
            if int(code) > 1000:
                raise HTTPException(status_code=401, detail="Unauthorized")
            return True
        except:
            raise HTTPException(status_code=401, detail="Unauthorized")

    def verify_websocket(self, websocket, db_session):
        """
        Check if the websocket is still valid.

        Parameters:
            websocket       [Websocket]         -   Websocket connection
            db_session      [Session]           -   database session

        Return:
                            [int]               -   id of the user
        """
        token = websocket.cookies.get('Authorization')
        code = validate_session(token, db_session)
        if int(code) > 1000:
            db_session.close()
            raise HTTPException(status_code=401, detail="Unauthorized")
        user_id = get_session_by_token(token, db_session).user_id
        db_session.close()
        return user_id

    def register(self, request):
        """
        Create an entry in the database for the new user

        Return
                            [Response]  -          Detailed API response
        """
        self.check_request(request, ['userName', 'displayName', 'password'])
        errors = []
        if any([len(request['userName']) < MIN_USER_LENGTH,
                len(request['userName']) > MAX_USER_LENGTH]):
            errors.append('3002')
        if any([len(request['password']) < MIN_PW_LENGTH,
                len(request['password']) > MAX_PW_LENGTH]):
            errors.append('3003')
        if any([len(request['displayName']) < MIN_USER_LENGTH,
                len(request['displayName']) > MAX_USER_LENGTH]):
            errors.append('3004')
        db_session = get_db().session
        if db_session.query(UserModel).filter(
            UserModel.name == request['userName']
        ).count() != 0:
            errors.append('1001')
        if len(errors) != 0:
            return self.create_error(errors)

        pw = str(hl.pbkdf2_hmac('sha256', request['password'].encode('utf-8'),
                                SALT, 100000))

        creation_date = datetime.datetime.now()
        user = UserModel(name=request['userName'],
                         display_name=request['displayName'],
                         password=pw,
                         creation_date=creation_date)

        db_session.add(user)
        db_session.commit()
        db_session.close()
        return self.create_response('0001')

    def login(self, request):
        """
        Check the credentials for the user and create a user_session if the
        credentials are valid

        Return:
                            [Response]  -          Detailed API response
        """
        self.check_request(request, ['userName', 'password'])
        db_session = get_db().session
        pw = str(hl.pbkdf2_hmac('sha256', request['password'].encode('utf-8'),
                                SALT, 100000))
        user = db_session.query(UserModel).filter(
            UserModel.name == request['userName'] and\
            UserModel.password == pw).first()
        if not user:
            return self.create_error(['1002'])

        token = create_token(request['userName'], pw)
        login_date = datetime.datetime.now()
        expire_date = login_date + datetime.timedelta(minutes=60)
        user_session = UserSessionModel(user_id=user.id, token=token,
                                        login_date=login_date,
                                        expire_date=expire_date)
        db_session.add(user_session)
        db_session.commit()

        data = {'user': {'userName': user.name,
                         'displayName': user.display_name,
                         'id': user.id}}
        response = self.create_response('0002', data)
        response.set_cookie(
            "Authorization",
            value=token,
            domain=os.environ['DOMAIN'],
            httponly=True,
            max_age=3600,
            expires=3600,
        )
        db_session.close()
        return response

    def check_login(self, token):
        """
        Check if the user_session is valid for the given token.

        Parameters:
            token           [str]           -   java-web-token

        Return:
                            [Response]      -   HTTP response
        """
        db_session = get_db().session
        code = validate_session(token, db_session)
        if int(code) > 1000:
            return self.create_error([code])
        user_id = get_session_by_token(token, db_session).user_id
        user = db_session.query(UserModel).filter(
            UserModel.id == user_id).first()

        data = {'user': {'userName': user.name,
                         'displayName': user.display_name,
                         'id': user.id}}
        db_session.close()
        return self.create_response('0007', data)

    def logout(self, token):
        """
        Invalidate the user_session for the given token and upsate the database
        entry with the logout date.

        Parameters:
            token           [str]               -   java-web-token

        Return:
                            [Boolean]           -   Whether the logout
                                                    was successful or not
        """
        db_session = get_db().session
        user_session = get_session_by_token(token, db_session)
        if not user_session:
            db_session.close()
            return self.create_error(['1003'])
        user_session.logout_date = datetime.datetime.now()
        db_session.commit()
        db_session.close()
        return self.create_response('0004')
