"""
    Authors: Jan Sallermann
"""

import os
import datetime
from packages.database.schema import (UserModel, UserSessionModel, MessageModel,
                                      UserChatroomModel)
from packages.database.database import get_db
from packages.controller import Controller
from packages.helper import (
    create_token, get_session_by_token, validate_session
)
from fastapi import HTTPException
from websocket import manager
from packages.constants.status_codes import STATUS_CODES
from packages.constants.constants import (SALT, MIN_PW_LENGTH, MIN_USER_LENGTH,
                                          MAX_USER_LENGTH, MAX_PW_LENGTH)
import hashlib as hl


class UserController(Controller):
    def __init__(self):
        super().__init__()

    def change_pw(self, request, token):
        """
        Change the password for the user.

        Parameters:
            request         [json]          -   HTTP request as json
            token           [str]           -   java-web-token

        Return:
                            [Response]      -   HTTP response
        """
        self.check_request(request, ['oldPassword', 'password'])
        errors = []
        if any([len(request['password']) < MIN_PW_LENGTH,
                len(request['password']) > MAX_PW_LENGTH]):
            errors.append('3003')
        db_session = get_db().session
        user = get_session_by_token(token, db_session).user
        old_pw = str(hl.pbkdf2_hmac(
            'sha256', request['oldPassword'].encode('utf-8'), SALT, 100000))
        if user.password == old_pw:
            new_pw = str(hl.pbkdf2_hmac(
                'sha256', request['password'].encode('utf-8'), SALT, 100000))
            user.password = new_pw
            db_session.commit()
            db_session.close()
            return self.create_response('0019')
        db_session.close()
        errors.append('1006')
        return self.create_error(errors)

    def update_user(self, request, token):
        """
        Change the data of the user.

        Parameters:
            request         [json]          -   HTTP request as json
            token           [str]           -   java-web-token

        Return:
                            [Response]      -   HTTP response
        """
        self.check_request(request, ['userName', 'displayName'])
        errors = []
        if any([len(request['userName']) < MIN_USER_LENGTH,
                len(request['userName']) > MAX_USER_LENGTH]):
            errors.append('3002')
        if any([len(request['displayName']) < MIN_USER_LENGTH,
                len(request['displayName']) > MAX_USER_LENGTH]):
            errors.append('3004')

        db_session = get_db().session
        user = get_session_by_token(token, db_session).user

        if request['userName'] != user.name and \
            db_session.query(UserModel).filter(
            UserModel.name == request['userName']
        ).count() != 0:
            errors.append('1001')

        if len(errors) != 0:
           return self.create_error(errors)

        user.name = request['userName']
        user.display_name = request['displayName']

        db_session.commit()

        data = {'user': {'userName': user.name,
                         'displayName': user.display_name,
                         'id': user.id}}

        db_session.close()
        return self.create_response('0022', data)

    async def delete_user(self, token):
        """
        Delete a user from the db.

        Parameters:
            token           [str]           -   java-web-token

        Return:
                            [Response]      -   HTTP response
        """
        db_session = get_db().session
        user = get_session_by_token(token, db_session).user

        for chat in user.chatrooms:
            await manager.remove_subscription(user.id, chat.chat_id)

        db_session.query(UserChatroomModel).filter(
            UserChatroomModel.user_id==user.id).delete()
        for message in user.messages:
            message.user_id = None

        db_session.delete(user)
        db_session.commit()
        db_session.close()
        return self.create_response('0023')
