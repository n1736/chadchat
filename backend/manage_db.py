#!/usr/bin/env python

"""
    Authors: Erik Luckner
"""

from migrate.versioning.shell import main
import os


DB = os.environ['DB_NAME']
DB_USER = os.environ['DB_USER']
DB_PW = os.environ['DB_PASSWORD']
DB_PORT = os.environ['DB_PORT']

if __name__ == '__main__':
    main(repository='migrations', url=f"mysql+pymysql://{DB_USER}:{DB_PW}@mariadb:{DB_PORT}/{DB}", debug='False')
