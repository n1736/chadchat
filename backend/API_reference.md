# CatCHAD API

## Standard Responses

### Successful responses

```json
{
    "message": "message describing the exit status of the API route",
    "code": "internal code representing the exit status of the API route"
}
```

### Error responses

```json
{
"errors": "list of all errors that occured"
}
```

**__Error:__**
```json
{
    "message": "message describing the exit status of the API route",
    "code": "internal code representing the exit status of the API route",
    "fields" "list of input field that raised the error (only returned if that is the case)"
}
```

### Object response models

**__User object:__**
```json
{
    "userName": "name of the user used to identify the user",
    "displayName": "name of the user that is displayed to other users",
    "id": "Id of the user in the database"
}
```

**__Chat object:__**
```json
{
    "id": "Id of the chatroom",
    "name": "name of the chatroom",
    "capacity": "maximum amount of mambers of the chatroom",
    "memberCount": "current amount of members of the chatroom",
    "publicChat": "Boolean that describes whether the chatroom is public or not",
    "description": "description of the chatroom",
    "userIsAdmin": "Boolean that describes whether the user that made the request is that admin of the chatroom or not"
}
```

**__Message object:__**
```json
{
    "id": "Id of the message",
    "chatId": "Id of the chatroom that the message has benn written in",
    "user": "user object",
    "content": "content of the message",
    "type": "type of the message (currently only text messages)",
    "createdAt": "Timestamp of the time the message was created at",
    "read": "Boolean whether the message has been read or not"
}
```

## Reference

- [Authentification](#authentification)
    + [POST /register](#register)
    + [POST /login](#login)
    + [GET /check-login](#check-login)
    + [POST /logout](#logout)
- [Chat-requests](#chat-requests)
    + [GET /chats](#get-chats)
    + [GET /chats/search](#search-chats)
    + [POST /chats/create](#create-chat)
    + [PATCH /chat/id](#update-chat)
    + [DELETE /chat/id](#delete-chat)
    + [GET /chat/id/members](#get-chatmembers)
    + [POST /chat/id/join](#join-chat)
    + [POST /chat/id/leave](#leave-chat)
    + [POST /chat/id/add-members](#add-chatmembers)
    + [POST /chat/id/remove-member](#remove-chatmember)
    + [POST /chat/id/search-available-users](#search-available-users)
- [User-requests](#user-requests)
    + [POST /change-password](#change-pw)
    + [PATCH /profile](#patch-profile)
    + [DELETE /profile](#delete-profile)
- [Message-requests](#message-requests)
    + [POST /messages/load-by-chat](#load-by-chat)

### Authentification <a name="authentification"></a>

#### POST /register <a name="register"></a>

Create an entry for the user in the database to enable further usage of the API.

**__Input__**
```json
{
    "userName": "Name that the user will be indentified by (min length 2, max length 30)",
    "displayName": "Name that will be shown to other users (min length 2, max length 30)",
    "password": "password of the user (min length 8)"
}
```

#### POST /login <a name="login"></a>

Create an active user session and set a cookie for the webuser which will be used for further authentification.

**__Input:__**
```json
{
    "userName": "valid username",
    "password": "valid password"
}
```

#### GET /check-login <a name="check-login"></a>

Checks if the session for the user is still active.

#### POST /logout <a name="logout"></a>

Close the current user session

### Chat-requests <a name="chat-requests"></a>

#### GET /chats <a name="get-chats"></a>

Get all chatrooms available to the user.

**__Output:__**
```json
{
    "chats": "list of chat objects"
}
```

#### GET /chats/search <a name="search-chats"></a>

Search through all chatrooms that the user isnt a part of.

**__Search-parameters:__**
```
amount: total amount of returned chats
offset: page of the returned chats
name: chatroom name that will be searched for
```

**__Output:__**
```json
{
    "chats": "list of chat objects",
    "totalResults": "total amount of results"
}
```

#### POST /chats/create <a name="create-chat"></a>

Create a chatroom.

**__Input:__**
```json
{
    "name": "Name of the chatroom (min length 2, max length 30)",
    "capacity": "maximum count of members for the chat",
    "publicChat": "Whether the chat is public or not (Boolean)",
    "description": "Description of the chatroom (max length 1kB)"
}
```

**__Output:__**
```json
{
    "chat": "chat object"
}
```

#### PATCH /chats/{id} <a name="update-chat"></a>

Update the data of a given chatroom. Executes only if the user is the admin of the chatroom.

**__Path-parameters:__**
```
id: Id of the chatroom
```

**__Input:__**
```json
{
    "name": "Name of the chatroom (min length 2, max length 30)",
    "capacity": "maximum count of members for the chat",
    "publicChat": "Whether the chat is public or not (Boolean)",
    "description": "Description of the chatroom (max length 1kB)"
}
```

**__Output:__**
```json
{
    "chat": "chat object"
}
```

#### DELETE /chats/{id} <a name="delete-chat"></a>

Delete a given chatroom. Executes only if the user is the admin of the chatroom.

**__Path-parameters:__**
```
id: Id of the chatroom
```

#### GET /chats/{id}/members <a name="get-chatmembers"></a>

Get all members of a given chatrooms.

**__Path-parameters:__**
```
id: Id of the chatroom
```

**__Search-parameters:__**
```
amount: total amount of returned users
offset: page of the returned users
```

**__Output:__**
```json
{
    "users": "list of user objects",
    "totalResults": "total amount of results"
}
```

#### POST /chats/{id}/join <a name="join-chat"></a>

Join a given chatroom.

**__Path-parameters:__**
```
id: Id of the chatroom
```

**__Output:__**
```json
{
    "chat": "chat object"
}
```

#### POST /chats/{id}/leave <a name="leave-chat"></a>

Leave a given chatroom. This is only possible if the user is not the admin of the chatroom.

**__Path-parameters:__**
```
id: Id of the chatroom
```

#### POST /chats/{id}/add-members <a name="add-chatmembers"></a>

Add one or multiple users to a given chatroom. Executes only if the user is the admin of the chatroom.

**__Path-parameters:__**
```
id: Id of the chatroom
```

**__Input:__**
```json
{
    "userIds": "List of all userIds that are to be added to the chatroom"
}
```

#### POST /chats/{id}/remove-member <a name="remove-chatmember"></a>

Remove one chatmember from the chatroom. Executes only if the user is the admin of the chatroom. Targets can only be non-admins.

**__Path-parameters:__**
```
id: Id of the chatroom
```

**__Input:__**
```json
{
    "userId": "userId of the user that is to be removed"
}
```

#### GET /chats/{id}/search-available-users <a name="search-available-users"></a>

Search through all users that could be added to the given chatroom.

**__Path-parameters:__**
```
id: Id of the chatroom
```

**__Search-parameters:__**
```
amount: total amount of returned chats
offset: page of the returned chats
name: username that will be searched for
```

**__Output:__**
```json
{
    "users": "list of user objects",
    "totalResults": "total amount of results"
}
```

### User-requests <a name="user-requests"></a>

#### POST /change-password <a name="change-pw"></a>

Change the password of the user that makes the request.


**__Input__**
```json
{
    "oldPassword": "current password of the user",
    "password": "new password of the user (min length 8)"
}
```

#### PATCH /profile <a name="patch-profile"></a>

Change the data of the user that makes the request.

**__Input__**
```json
{
    "userName": "Name that the user will be indentified by (min length 2, max length 30)",
    "displayName": "Name that will be shown to other users (min length 2, max length 30)"
}
```

**__Output:__**
```
{
    "user": "user object"
}
```

#### DELETE /profile <a name="delete-profile"></a>

Delete the user that makes the request.

### Message-requests <a name="message-requests"></a>

#### POST /messages/load-by-chat <a name="load-by-chat"></a>

Loads a certain amount of messages for a given chatroom that are older than a given message.

**__Input:__**
```json
{
    "chatId": "Id of the chatroom",
    "oldestMessageLoaded": "Id of the oldest message that has been loaded",
    "amount": "amount of messages that are to be loaded"
}
```

**__Output:__**
```
{
    "messages": "list of message objects"
}
```
