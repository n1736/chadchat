# CatCHAD

## Project setup

### Docker

-   Copy the _.env.example_ to _.env_ and adjust as needed.

Make sure [Docker](https://www.docker.com/get-started/) (and docker-compose) is installed and the deamon is running.

_Docker needs root privileges, to avoid having to prefix sudo to every docker command, add your user to the docker group._

Start and build the containers:

```console
$   docker-compose up -d --build
$   # To avoid building each time, omit the build-flag when not necessary.
```

Stop the containers:

```console
$   docker-compose down
```

### Backend

-   All required apt-packages belong into _backend/pkglist.txt_.
-   All required python-packages belong into _backend/requirements.txt_.

_Rebuilding the python container after altering these files is required._

The README for the API can be found [here](backend/API_reference.md).

### Execute migrations

To execute database migrations, see the [Migration-README](backend/migrations/README.md).

### Frontend

See the [Frontend-README](frontend/README.md).
