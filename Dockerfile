### Authors: Dominic Martin

FROM python:3

WORKDIR /catchad

COPY ./backend/pkglist.txt .
RUN apt-get install -y --no-install-recommends $(cat pkglist.txt)

RUN pip install --upgrade pip

COPY ./backend/requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt